/**
 * Created by sahid on 5/17/16.
 */
jQuery(window).load(function() {

    show_reviews();
    setInterval(function(){
        show_reviews();
    },5000);

    jQuery(document).on('click','.load_more_post .next a', function(e) {
        e.preventDefault();

        var MainCat =  $("select#navMenuList").val();
        var DiffCat = $("select#category_diff").val();
        var urls = $(this).attr('href');
        var parents = $(this).parents('.load_more_post');
        $(this).parent().hide();
        var load = parents.find('.main_load');
        var order = $("#navMenuList").val();
        $(this).remove();
        if(order=='0')
            order=1;
         $.ajax({
            url: urls,
            type: 'post',
            data:{'order':order,'main_cat':MainCat,'diff_cat':DiffCat},
            beforeSend: function () {
                load.fadeIn();
            },
            complete: function () {
                parents.remove();
            },
            success: function (data) {
                $('.all_post').append(data);

                $(".ajax_post").slideDown();
                parents.remove();
            },
            error: function (request, error) {
                // This callback function will trigger on unsuccessful action
                console.log("An error occurred: " + status + "nError: " + error);
            }
        });
    });





})


function show_reviews(){

    var reviewlistUrl = $(".sp-review-btn").find('a').attr('href');

    var defaultImgSrc = root+'img/no-image.png';
     $.ajax({
        url: url+'/reviews',
        type: 'post',
        dataType: 'html',
        beforeSend: function () {
            // jQuery(product.loader).fadeIn();
            //$('.review').fadeOut('fast');
        },
        complete: function () {
            //jQuery(product.loader).fadeOut();
            //$('.review').fadeIn('fast');
        },
        success: function (data) {
            console.log(data);
            var obj = JSON.parse(data.toString());

            var html = '';
            if(obj.error==false){

                var imgsrsc = (obj.message.student_group.image!=null) ? (obj.message.student_group.image.length!=0) ?  obj.message.student_group.image : defaultImgSrc : defaultImgSrc;

                imgsrsc = 'http://webhawks.oceanize.co.jp/smart_campus/' + imgsrsc

                html += '<div class="review_inner"> ' +
                '<div class="review_img"> <img src="'+obj.final_img+'"/> <p class="text-center review-list-btn">' +
                    '<a href="'+reviewlistUrl+'"> 利用者の声</a></p> </div> ' +
                '<div class="review_content"> ' +
                '<h3 class="title">  ' + obj.message.univ.name + '  </h3> ' +
                '<p class="squre"> ' + obj.message.student_group.name + ' </p> ' +
                '<h3 class="title_2"> '+obj.message.title.substr(0, 10)+' ...'+  '</h3> ' +
                '<p class="circle"> '+obj.message.description.substr(0, 45)+' ...'+  '</p> ' +
                '</div> ' +
                '</div>';

            }else {
                html = 'error';
            }
            if(html!='' && html!='error')
                $('.review').html(html);
            else
                $('.review').css('display','none');
         },
        error: function (request, error) {
            // This callback function will trigger on unsuccessful action
            //alert("An error occurred: " + status + "nError: " + error);
        }
    });
}
/*
* Auto load content when scroll
* */
var processScroll = true;
$(window).scroll(function(){
    if ( processScroll  && $(window).scrollTop() == $(document).height() - $(window).height()){
        processScroll = false;
        console.log($(document).height() - $(window).height());
        $(document).find(".load_more_post .next a").trigger("click");
        processScroll = true;
    }
});


$(window).ready(function(){

    $(".cat_search_btn").on("click", function(e){
        e.preventDefault();
        var MainForm = $(this).parents("form");
        var MainCat =  MainForm.find("select#navMenuList").val();
        var DiffCat = MainForm.find("select#category_diff").val();
        var load = $(".main_load");

        $.ajax({
            url: url + '/index',
            type: 'post',
            data:{'main_cat':MainCat,'diff_cat':DiffCat},
            dataType: 'html',
            beforeSend: function () {
                load.fadeIn();
            },
            complete: function () {
                load.hide();
            },
            success: function (data) {
                $('.all_post').html(data);
                $(".ajax_post").slideDown('fast');
            },
            error: function (request, error) {
            }
        });

    });
    $(document).on('change','.MobileView select#navMenuList, .MobileView select#category_diff', function(e){

        e.preventDefault();
        var MainForm = $(this).parents("form");
        var MainCat = MainForm.find("select#navMenuList").val();
        var DiffCat = MainForm.find("select#category_diff").val();
        var load = $(".main_load");

        $.ajax({
            url: url + '/index',
            type: 'post',
            data: {'main_cat': MainCat, 'diff_cat': DiffCat},
            dataType: 'html',
            beforeSend: function () {
                load.fadeIn();
            },
            complete: function () {
                load.hide();
            },
            success: function (data) {
                $('.all_post').html(data);
                $(".ajax_post").slideDown('fast');
            },
            error: function (request, error) {
            }
        });

    })

    changeClassOnResize()

    $( window ).resize(function(){
        changeClassOnResize();
    });
})




function changeClassOnResize() {
    var deviceWidth = $(window).width();
    if (deviceWidth > 990) {
        $(".search-form-holder").find('form').addClass('DesktopView');
        $(".search-form-holder").find('form').removeClass('MobileView');
    }else {
        $(".search-form-holder").find('form').removeClass('DesktopView');
        $(".search-form-holder").find('form').addClass('MobileView');
    }
}

