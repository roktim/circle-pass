/**
 * Created by sahid on 5/12/16.
 */
jQuery(document).ready(function(){

     $('.banner_slide').owlCarousel({
        margin:0,
        loop:true,
        autoWidth:false,
        dots:false,
        nav:true,
        responsiveClass:true,
        autoplay: true,

        navText:['&nbsp;','&nbsp;'],
         items:1,
        singleItem: true,
    });
    $('.detail_banner_slide').owlCarousel({
        margin:0,
        loop:true,
        center:true,
        autoWidth:false,
        autoHeight:true,
        dots:false,
        nav:true,
        responsiveClass:true,
        autoplay: true,
        navText:['&nbsp;','&nbsp;'],
        items:1,
        singleItem: true
    });
    /*
    * Default Modal
    * */

    $.fn.oc_modal = function(options) {
        var defaults = {

        }
        var settings = $.extend( {}, defaults, options );

        var modal_target = $(this).data('target');
        var myModal = $(modal_target),
            main_modal = $(myModal).find('.modal_main');

        $(this).click(function(e){
             e.preventDefault();

            myModal.fadeIn('fast', function(){
                main_modal.stop().animate({
                    'margin-top':'23%'
                },500)
            });

        });
        //Close Modal
        $('.close_modal').click(function(){
            main_modal.stop().animate({
                'margin-top':'-100%'
            },500, function(){
                myModal.fadeOut();
            });
        })

    };
    // modal close
    $.fn.oc_modal_close = function(options) {

        var defaults = {
            close_cls:'.close_modal',
            myModal:'.modal_main'
        }
        var settings = $.extend({}, defaults, options);
        //Close Modal
        var that = $(this);
            $(this).find(settings.myModal).stop().animate({
                'margin-top':'-100%'
            },500, function(){
                that.fadeOut();
            });
    }
    // modal show
    $.fn.oc_modal_show = function(options) {

        var defaults = {
            myModal:'.modal_main'
        }
        var settings = $.extend({}, defaults, options);
        //Show Modal
        $(this).fadeIn('fast', function(){
            $(settings.myModal).animate({
                'margin-top':'23%'
            },500)
        });


    }

    //About Modal
    $(".show_review_modal").oc_modal();

    $(".show_confirm_modal").oc_modal();
    $(".show_signup_modal").oc_modal();
    $(".invite_friend_modal").oc_modal();

    /*
    * If not login
    * */
    //Manually Close Modal
     $(".close_modal").click(function () {
     $(".modal_main").stop().animate({
     'margin-top':'-100%'
     },500, function(){
     $(".modal_shadow").css("display",'none');
     });
     });

    $(".Go_to_entry").click(function(){
        var dialog = "Circle passに申し込むにはログインする必要があります";
        if($("#user_id").val() == ""){
            $("#alert_msg").empty().append(dialog);
            $("#signup_modal").oc_modal_show();
        }
        else if($('#permission').val()==0){

             $("#less_point").css('display','none');
            $("#less_rank").empty().append("お使いのアカウントには応募する権限がありません。団体の管理者へのご確認をお願い致します。");
            $("#less_campus_point").oc_modal_show();
        }
        else if($("#student_group_rate").val() < $("#content_rank").val()){
            $("#less_point").css('display','none');
            $("#less_rank").empty().append("このコンテンツをご利用頂くにはランクが足りません");
            $("#less_campus_point").oc_modal_show();
        }
        else if($("#campus_point").val() < $("#content_point").val()){
            $("#less_rank").css('display','none');
            $("#less_campus_point").oc_modal_show();
        } else {
            window.location.href = $('#entry_url').attr('href');
        }

    });
    /*
     * Content details Loadmore related content
     * @UB
     * */


    $(function () {
        $(".related_post").slice(0, 3).show();
        $("#content_loadmore").on('click', function (e) {
            $(".load_more").hide();
            e.preventDefault();
            $(".related_post:hidden").slice(0, -1).slideDown();
            if ($(".related_post:hidden").length == 0) {
                $("#content_loadmore").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
         //$(".load_more").addClass( "hidden_btn" );
        });

    });

    /*
    *   Submit confirm and show thanks popup
    * */
$("#entry_log").click(function(){
    var url = $("#entry_url").val();
    var title = $("#content_title").val();
    var point = $("#content_point").val();

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            title: title,point: point
        },
      /*  beforeSend: function() {
            $(".modal").show();
        },
        error: function () {
            $('.collect-user-info-container').html('<p>An error has occurred</p>');
        },
        complete: function() {
            $(".modal").hide();
        },*/
        success: function (data, textStatus, request) {
            //$("#confirm_modal").oc_modal_close();
            $("#confirm_modal").css("display","none");
           $("#thanks_modal").oc_modal_show();
        }
    });

});
    $(window).load(function() {
        $("#status").fadeOut();
        $("#preloader").delay(1000).fadeOut("slow");
    });


    // Auto tab
    var tabChange = function () {
        var tabs = $('.tab_lists > li');
        var active = tabs.filter('.active');
        var next = active.next('li').length ? active.next('li') : tabs.filter(':first-child');
        // Use the Bootsrap tab show method
        next.tab('show');
    };
    // Tab Cycle function
    var tabCycle = setInterval(tabChange, 8000);
    //End Auto Tab
    /*
    * Next tab
    * */
    $('.tabNext').click(function(){
        $('.tab_lists > .active').next('li').trigger('click');
    });
    $('.tabPrev').click(function(){
        $('.tab_lists > .active').prev('li').trigger('click');
    });

    $(".tab_lists li").click(function(){
        var time = 500;
        var tabTarget = $(this).data('target');
        var tabContent = $(".tabs-content");
        $(this).addClass('active');
        $(this).nextAll().removeClass('active');
        $(this).prevAll().removeClass('active');
        tabContent.find(tabTarget).nextAll().fadeOut(0).removeClass('active');
        tabContent.find(tabTarget).prevAll().fadeOut(0).removeClass('active');

        tabContent.find(tabTarget).fadeIn(time, function(){
            $(this).addClass('active');
        })
        clearInterval(tabCycle);
        /*clearInterval(tabCycle);
        setTimeout(function () {
            tabCycle = setInterval(tabChange, 8000);
        }, 8000);*/
    });

    $(".show_user_info").oc_modal();





    /*
        Check the last priority changed from the student group form
        And send the last changed field data for registration studentgroup
     */
    $("#studentGroupRegisterForm .studentGroupList").on('change', function(e){
        var Value = $(this).val();
        var optionText = $(this).find("option:selected").text();
        var InputValue = $(this).parents('form').find("input[name=organization]").val();
        $(this).addClass('last_priority');


        if(Value=='0') {
            $(this).parents('.form-group').addClass('text-muted');
            $(this).removeClass('last_priority');
            $(".show_recent_group_name").html('');
            if(InputValue=='') {
                $(this).parents('form').find('input[name=organization]').removeClass('last_priority');

                $(this).parents('.form-group').next().addClass('text-muted');
            }else {
                $(this).parents('form').find('input[name=organization]').addClass('last_priority');
                $(this).parents('.form-group').next().removeClass('text-muted');
            }
        }else {
            $(this).parents('.form-group').removeClass('text-muted');
            $(this).parents('.form-group').next().addClass('text-muted');
            $(this).parents('form').find('input[name=organization]').removeClass('last_priority');
            $(".show_recent_group_name").html('<p> '+optionText+' に登録されます </p>');
        }
    })

    $("#studentGroupRegisterForm input[name=organization]").on('keyup keypress blur change', function(e){
        var Value = $(this).val();
        var SelectValue = $(this).parents('form').find(".studentGroupList");
        //Show message
        $(this).addClass('last_priority');


        $(this).parents('form').find(".studentGroupList option[value=0]").attr('selected','selected');



        if(Value=='') {
            $(this).parents('.form-group').addClass('text-muted');
            $(this).removeClass('last_priority');
            $(".show_recent_group_name").html('');
            if(SelectValue.val()=='0') {
                $(this).parents('.form-group').prev().addClass('text-muted');

            }else {
                $(this).parents('.form-group').prev().removeClass('text-muted');
                $(this).parents("form").find(".studentGroupList").addClass("last_priority");
            }

        }else {
            $(this).parents('.form-group').removeClass('text-muted');
            $(this).parents('.form-group').prev().addClass('text-muted');
            $(this).parents('form').find('.studentGroupList').removeClass('last_priority');
            $('.studentGroupList').selectpicker('val', '0');
            $(".show_recent_group_name").html('<p> '+Value+' を新規作成します </p>');
        }
    })

    $("#studentGroupRegisterForm").on('submit', function(){

            var GroupID = $(this).find('select.last_priority').val();
            var NewGroupName = $(this).find("input.last_priority").val();

            if(typeof GroupID === 'undefined' && typeof NewGroupName ==='undefined' ) {
                GroupID = '0'; NewGroupName = "";
            }else {
                if (typeof GroupID === 'undefined') {
                    GroupID = '0';
                }
                if (typeof NewGroupName === 'undefined') {
                    NewGroupName = "";
                }
            }


        var beforeRegister = $(this).parents('.modal_body').find('.beforeRegister'),
            afterRegister = $(this).parents('.modal_body').find('.afterRegister'),
            loader = $(this).parents('.modal_body').find('#preloader');


        if(NewGroupName.length>1 || GroupID!='0'){

            $(this).find('.from-group').removeClass('has-error');

            loader.fadeIn('fast');
            $.ajax({
                url: url+'/registerStudentGroup',
                type: 'post',
                data:{'NewGroupName': NewGroupName, 'GroupID':GroupID},
                dataType: 'json',
                success: function (data) {

                     if(data.success==true){
                        $(".group_exists").hide();

                        beforeRegister.fadeOut(100, function(){
                            afterRegister.fadeIn('slow');
                        });
                        loader.fadeOut('fast');
                    }else {
                        console.log("Internal problem occured member registration");
                    }

                },
                error: function (request, status, error) {
                }
            });

        }else {
            $(this).find('.form-group').addClass('has-error');
        }
        return false;
    })
    /*
    * Hide Popup when click outside of the popup
    * */
    $(document).click(function(event ){
        var $target = $(event.target);
        if($target.is(".modal_shadow")){
           $("body").find(".modal_shadow").hide();
        }
    });


    $(".group_exists a").on('click',function(e){
        e.preventDefault();
        $("#user_info").oc_modal_close();

        setTimeout(function(){
            $("#group_notification").oc_modal_show();
        },1000)


    });




    /*
        Change the select box text in PC and SP
     */

    var sp1 = '役割全て',
        sp2 = '悩み全て',
        pc1 = '全ての役割から探す',
        pc2 = '全ての悩みから探す';

    var documentWidth = $(document).width();

    if(documentWidth<768){
        $('#navMenuList option[value="0"]').text(sp1);
        $('#category_diff option[value="0"]').text(sp2);
    }else {
        $('#navMenuList option[value="0"]').text(pc1);
        $('#category_diff option[value="0"]').text(pc2);
    }

    $(window).resize(function(){
        var documentWidth = $(document).width();

        if(documentWidth<768){
            $('#navMenuList option[value="0"]').text(sp1);
            $('#category_diff option[value="0"]').text(sp2);
        }else {
            $('#navMenuList option[value="0"]').text(pc1);
            $('#category_diff option[value="0"]').text(pc2);
        }
    })
});

$( ".file_up_pc" ).on( "click", function() {
    $( "#userImage" ).trigger( "click" );
});
$( ".file_up_sp" ).on( "click", function() {
    $( "#userImage_sp" ).trigger( "click" );
});


$(".inputFile").on('change',(function(e) {
        $('.upload-preview').attr('data-image','aa');
        e.preventDefault();
        var input = document.getElementById("userImage");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#targetLayer").html('<img src="'+e.target.result+'" class="banner_img_sp"  />');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }));

$(".edit-form-submit").click(function() {

    var img_attr= $(".upload-preview").attr('data-image');

    if(img_attr==''){

    var description= $("#description").val();
    var member_number= $("#member_number").val();
    var location= $("#location").val();
    var form_name= $("#form_name").val();
    var type_name= $("#type_name").val();
    var category_id= [];
    $(".checkbox:checked").each(function (argument) {
       category_id.push($(this).val());
    })
    var phone_number= $("#phone_number").val();
    var facebook_url= $("#facebook_url").val();
    var line_name= $("#line_name").val();
    var email= $("#email").val();
    var twitter_url= $("#twitter_url").val();
    var official_url= $("#official_url").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: root+"home/profile_update",
        data: {
           description:description,
           member_number:member_number,
           location:location,
           form_name:form_name,
           type_name:type_name,
           category_id:category_id,
           phone_number:phone_number,
           facebook_url:facebook_url,
           line_name:line_name,
           email:email,
           twitter_url:twitter_url,
           official_url:official_url
        },
        beforeSend : function(){
        $('.modal').show();
        },
        success: function(data) {
         $('.modal').hide();
         $("#confirmation").oc_modal_show();
        }
    });

    }else{

    var group_image= $(".upload-preview").attr('src');
    var description= $("#description").val();
    var member_number= $("#member_number").val();
    var location= $("#location").val();
    var form_name= $("#form_name").val();
    var type_name= $("#type_name").val();
    var category_id= [];
    $(".checkbox:checked").each(function (argument) {
       category_id.push($(this).val());
    })
    var phone_number= $("#phone_number").val();
    var facebook_url= $("#facebook_url").val();
    var line_name= $("#line_name").val();
    var email= $("#email").val();
    var twitter_url= $("#twitter_url").val();
    var official_url= $("#official_url").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: root+"home/profile_update",
        data: {
           photo:group_image,
           description:description,
           member_number:member_number,
           location:location,
           form_name:form_name,
           type_name:type_name,
           category_id:category_id,
           phone_number:phone_number,
           facebook_url:facebook_url,
           line_name:line_name,
           email:email,
           twitter_url:twitter_url,
           official_url:official_url
        },
        beforeSend : function(){
        $('.modal').show();
        },
        success: function(data) {
         $('.modal').hide();
         $("#confirmation").oc_modal_show();
        }
    });
    }

   

});

$(".inputFile_sp").on('change',(function(e) {
        e.preventDefault();
        var input = document.getElementById("userImage_sp");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#targetLayer_sp").html('<img src="'+e.target.result+'" class="banner_img_sp" />');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }));

$(".edit-form-submit-sp").click(function() {
    var group_image= $("#targetLayer_sp img").attr('src');
    var description= $("#description_sp").val();
    var member_number= $("#member_number_sp").val();
    var location= $("#location_sp").val();
    var form_name= $("#form_name_sp").val();
    var type_name= $("#type_name_sp").val();
    var category_id_sp= [];

    $(".checkbox_sp:checked").each(function (argument) {
       category_id_sp.push($(this).val());
    })
    var phone_number= $("#phone_number_sp").val();
    var facebook_url= $("#facebook_url_sp").val();
    var line_name= $("#line_name_sp").val();
    var email= $("#email_sp").val();
    var twitter_url= $("#twitter_url_sp").val();
    var official_url= $("#official_url_sp").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: root+"home/profile_update",
        data: {
           photo:group_image,
           description:description,
           member_number:member_number,
           location:location,
           form_name:form_name,
           type_name:type_name,
           category_id:category_id_sp,
           phone_number:phone_number,
           facebook_url:facebook_url,
           line_name:line_name,
           email:email,
           twitter_url:twitter_url,
           official_url:official_url
        },
        beforeSend : function(){
        $('.modal').show();
        },
        success: function(data) {
         $('.modal').hide();
         $("#confirmation_sp").oc_modal_show();
        }
    });
});

/*Number only fields*/
$(".number").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

$("#email").blur(function() {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailaddress = $("#email").val();
    if(!emailReg.test(emailaddress))
        $(this).css('color','#FF3838');
    else
        $(this).css('color','#555555');
});

$(".url").blur(function() {
    var emailReg = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
    var emailaddress = $(this).val();
    if(!emailReg.test(emailaddress))
        $(this).css('color','#FF3838');
    else
        $(this).css('color','#555555');
});

$( ".file_up_pc" ).on( "click", function() {
    $( "#userImage" ).trigger( "click" );
});

$( ".file_up_sp" ).on( "click", function() {
    $( "#userImage_sp" ).trigger( "click" );
});
