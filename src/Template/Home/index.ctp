<script type="text/javascript">
    var url = '<?php echo $this->Url->build(["controller" => "Home"]) ?>';
    var root = '<?php echo $this->Url->build('/');  ?>';
</script>

<?php
$this->start('script');
    echo $this->Html->script('reviews', array('inline' => false));
$this->end();
$this->start('title');
    echo $title;
$this->end();

$this->start('banner');
    echo $this->element('banner');
$this->end();

$this->start('navmenu');
    echo $this->element('navbar/nav-menu');
$this->end();
?>

<div class="row main_home">
    <div class="main_load">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <div class="all_post">
        <!-- /.main_load -->
        <?php if(count($contents)>0):
            if(!$this->request->is('ajax')):
                foreach($contents as $content):
            ?>
        <div class="col-md-4 col-sm-6 col-xs-6 post">
            <a href="<?= $this->Url->build(["controller"=>"Home", "action"=>'details',$content->id]) ?>">
            <div class="post_item">
                <div class="post_thumb">
                   <?php if($content->tag): ?> <div class="item_label"> <?= $content->tag; ?> </div> <?php endif; ?>
                    <!-- /.item_label -->

                        <?php if($content->image): ?>
                        <img class="img-responsive" src="<?= $content->image; ?>" alt="<?= $content->title ?>">
                    <?php else:
                            echo $this->Html->image('post_2.jpg',array('class'=>'img-responsive','alt'=>'No Image'));
                        endif; ?>

                    <div class="item_title">
                        <?php if($content->tag) {$subTitleLimit = 50;} else {$subTitleLimit = 60;}  ?>
                         <span class="title"><?php if(mb_strlen($content->title,'UTF-8') > $subTitleLimit): echo mb_substr($content->title, 0, $subTitleLimit,'UTF-8')."…"; else: echo $content->title;  endif; ?></span>
                         <!-- /.title -->
                       <?php if($content->tag): ?> <span class="percentage"> <?= $content->tag; ?> </span> <?php endif; ?>
                        <!-- /.percentage -->
                    </div>
                    <!-- /.item_title -->
                </div>
                <!-- /.post_thumb -->
                <div class="post_desc">
                    <div class="item_title">
                        <?php if($content->tag) {$subTitleLimit = 10;} else {$subTitleLimit = 10;}  ?>
                        <span class="title"><?php if(mb_strlen($content->title,'UTF-8') > $subTitleLimit): echo mb_substr($content->title, 0, $subTitleLimit,'UTF-8')."…"; else: echo $content->title;  endif; ?></span>
                        <!-- /.title -->
                        <?php if($content->tag): ?> <span class="percentage"> <?= $content->tag; ?> </span> <?php endif; ?>
                        <!-- /.percentage -->
                    </div>
                    <!-- /.item_title -->
                    <div class="post_content">  <?php $des = $content->description; if( mb_strlen ($des,'UTF-8') > 36): echo mb_substr($des, 0, 36,'UTF-8')."…"; else: echo $des;  endif; ?> </div>
                    <div class="post_action">
                        <div class="col-md-3 col-sm-3 col-xs-3 no-padding-sp padding-left-0 padding-right-0">
                            <div class="con_sup_img">
                                <?php if($content['supporter']['image']): ?>
                                <img src="<?= $content['supporter']['image']; ?>" alt="<?= $content['supporter']['name']; ?>">
                                <?php else:
                                        echo "企業ロゴ";
                                        endif; ?>
                            </div>
                            <!-- /.button -->
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-7 no_pad_left no_pad_right">
                            <div class="support_by">
                                <p> supported by </p>
                                <h4 class="sup_pc"><?= $content['supporter']['name']; ?></h4>
                                <h4 class="sup_sp"><?= mb_substr($content['supporter']['name'],0,5)."..."; ?></h4>

                            </div>
                            <!-- /.support_by -->
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 no_pad_left no_pad_right post_rank">
                            <?php
                           
                            if($content['rank']==1){
                                $rank_img = 'modal_lg_logo_1.png';
                            }
                            else if($content['rank']==2){
                                $rank_img = 'modal_lg_logo_2.png';
                            }
                            else if($content['rank']==3){
                                $rank_img = 'modal_lg_logo_3.png';
                            }
                            else if($content['rank']==4){
                                $rank_img = 'modal_lg_logo_4.png';
                            }
                            else if($content['rank']==5){
                                $rank_img = 'modal_lg_logo_5.png';
                            }

                            ?>
                            <?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image'));?>
                        </div>
                        <!-- /.col-md-2 col-sm-2 col-xs-2 no_pad_left no_pad_right -->

                    </div>
                    <!-- /.post_action -->
                </div>
                <!-- /.post_desc -->
            </div>
            <!-- /.post_item -->
            </a>
        </div>

        <?php endforeach; endif; ?>

        <?php  if($this->Paginator->counter('{{pages}}')>1): ?>

        <div class="col-md-12 load_more_post">
            <div class="main_load">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
             <!-- /.load_gif -->
              <?php echo $this->Paginator->next('もっと見る'); ?>
         </div>
        <!-- /.col-md-12 -->
        <?php endif; endif; ?>
    </div>
</div>
<!--Main Home Page    -->
