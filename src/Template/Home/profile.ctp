<div class="row main_home home_details">
  <div class="row banner">
          <div class="col-md-12 banner_sp">
          <div class="item detail_banner dtl_banner_sp" style="background-image: url(<?php echo $details[0]['image_url']; ?>);">
              <!-- image in style sheet -->
          </div>
          <!-- /.item -->
      </div>
      </div>

      <div class="main_content">
        <div class="row details_top">
          <div class="text_part fs_17">
            <h3 class="title fs_16"> <?php echo $details[0]['name']; ?></h3>
            <p><?php if(!empty($details[0]['univ']['name'])) { echo $details[0]['univ']['name']; } ?></p>
          </div>
          <div class="social_icon">
            <a target="_blank" <?php if(empty($details[0]['facebook_url'])){ ?> style="opacity: .2" <?php } ?> class="fb"  href="<?php echo $details[0]['facebook_url'];  ?>"><img src="/img/social_icon/fb.png" alt=""></a>
            <a target="_blank" <?php if(empty($details[0]['twitter_url'])){ ?> style="opacity: .2" <?php } ?> class="twitter" href="<?php echo $details[0]['twitter_url'];  ?>"><img  src="/img/social_icon/twitter.png" alt=""></a>
            <a target="_blank" <?php if(empty($details[0]['official_url'])){ ?> style="opacity: .2" <?php } ?> class="text_icon" href="<?php echo $details[0]['official_url'];  ?>"><img src="/img/social_icon/text_icon.png" alt=""></a>
          </div>
          </div>

          <div class="row grade_category">
            <div class="col-md-2 col-sm-3 col-xs-5 details_rank dtl_rnks">
                <span class="ltg"> Grade  </span>
                <?php if(!empty($details[0]['rate'])) { ?>
                <img src="/img/social_icon/rank_<?php echo $details[0]['rate']; ?>.png" class="profile_rank pro_rnk" alt="Rank Image">
                <?php } ?>
              </div>

            <div class="col-md-9 col-xs-7 rank_cat_tab">
                <span class="ltc">Category</span>
                <div class="tab_menu">
                  <ul>
                    <?php if(!empty($cat_name)) { for( $i=0; $i<count($cat_name); $i++ ){  ?>
                    <li><a href="#"><?php echo $cat_name[$i]; ?></a></li>
                    <?php } } ?>
                  </ul>
                </div>
            </div>
          </div>
          <div class="row details_top">
            <div class="col-md-6 details_para">
              <p><?php if(!empty($details[0]['description'])) { echo $details[0]['description']; } ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="social_icon_sp">
                <a target="_blank" <?php if(empty($details[0]['facebook_url'])){ ?> style="opacity: .2" <?php } ?> class="fb"  href="<?php echo $details[0]['facebook_url'];  ?>"><img src="/img/social_icon/sp/fb.png" alt=""></a>
                <a target="_blank" <?php if(empty($details[0]['twitter_url'])){ ?> style="opacity: .2" <?php } ?> class="twitter" href="<?php echo $details[0]['twitter_url'];  ?>"><img  src="/img/social_icon/sp/twitter.png" alt=""></a>
                <a target="_blank" <?php if(empty($details[0]['official_url'])){ ?> style="opacity: .2" <?php } ?> class="text_icon" href="<?php echo $details[0]['official_url'];  ?>"><img src="/img/social_icon/sp/text_icon.png" alt=""></a>
              </div>
            </div>
          </div>


      </div>
</div>
