<?php
$this->start('reviewheader');
    ?>
<div class="review-title">
    <h3 class="title"> 利用者の声 </h3>
    <!-- /.title -->
</div>
<?php
$this->end();
?>
<div class="row home_reviewslist">
    <!-- /.review-title -->
    <div class="review-lists">
        <?php if(count($allReviews)>0):
            foreach($allReviews as $review ):
         ?>
        <div class="review-list">
            <div class="col-md-4 col-sm-4 col-xs-4 review-media">
                <a href="<?= $this->Url->build(["controller"=>"Home", "action"=>'profile',$review['student_group_id']]) ?>">
                  <img class="img-responsive" src="<?php echo $review['student_group']['image_url']; ?>" alt="<?php echo $review['student_group']['image_alt']; ?>" />
                </a>
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-8 col-sm-8 col-xs-8 review-content">
                <h3 class="review-content-title"> <?php echo $review['student_group']['name']; ?> </h3>
                <!-- /.review-content-title -->
                <p class="review-content-details"> <?php echo $review['description'] ?></p>
            </div>
            <!-- /.col-md-8 content -->
        </div>
        <!-- /.review-list -->
        <?php endforeach; endif; ?>
    </div>
    <!-- /.review-lists -->
</div>
