
<?php
$this->start('banner');
echo $this->element('banner_details');
$this->end();

$this->start('navmenu');
echo $this->element('navbar/entry-details');
$this->end();
?>

<div class="row main_home home_details">



    <div class="points_sec hidden-xs">
        <p class="point_label"> こんなサークルにおすすめ:  <?php if(!empty($details->recommend_text)): echo $details->recommend_text; endif; ?></p>
    </div>

    <div class="points_sec hidden-xs">
        <p class="point_label"> こんな効果があるかも: <?php if(!empty($details->effect_text)):   echo $details->effect_text; endif; ?></p>
    </div>
    <article class="content hidden-xs">
        <p><?php if(!empty($details->description)): echo $details->description; endif; ?></p>
    </article>
    <div class="row recent_posts hidden-xs">
        <?php if(!empty($details->sub_image_1)): ?>
        <div class="col-md-4 col-sm-4 col-xs-12 post">
            <div class="post_inner post_item">
                <div class="post_thumb">
                    <img class="img-responsive" src="<?= str_replace(' ','%20',$details->sub_image_1); ?>" alt="Sub Image One w"/>
                </div>
                <!-- /.post_thumb -->
                <div class="details">
                    <p><?php if(!empty($details->sub_description_1)): echo $details->sub_description_1; endif; ?></p>
                </div>
                <!-- /.details -->
            </div>
            <!-- /.post_inner -->
        </div>
        <?php endif; ?>
        <!-- /.col-md-4 -->
        <?php if(!empty($details->sub_image_2)): ?>
        <div class="col-md-4 col-sm-4 col-xs-12 post">
            <div class="post_inner post_item">
                <div class="post_thumb">
                    <img class="img-responsive" src="<?= $details->sub_image_2; ?>" alt="Sub Image One"/>
                </div>
                <!-- /.post_thumb -->
                <div class="details">
                    <p><?php if(!empty($details->sub_description_2)): echo $details->sub_description_2; endif; ?></p>
                </div>
                <!-- /.details -->
            </div>
            <!-- /.post_inner -->
        </div>
        <?php endif; ?>
        <!-- /.col-md-4 -->
        <?php if(!empty($details->sub_image_3)): ?>
        <div class="col-md-4 col-sm-4 col-xs-12 post">
            <div class="post_inner post_item">
                <div class="post_thumb">
                    <img class="img-responsive" src="<?= $details->sub_image_3; ?>" alt="Sub Image One"/>
                </div>
                <!-- /.post_item -->
                <div class="details">
                    <p><?php if(!empty($details->sub_description_3)): echo $details->sub_description_3; endif; ?></p>
                </div>
                <!-- /.details -->
            </div>
            <!-- /.post_inner -->
        </div>
        <?php endif; ?>
        <!-- /.col-md-4 -->
    </div>
    <!-- /.col-md-12 -->

    <h3 class="user_circle_title hidden-xs">申し込み手続き</h3>
    <h3 class="user_circle_title text-center hidden-sm hidden-sm hidden-lg">申し込み手続き</h3>

    <div class="row points_panel">
        <div class="col-md-offset-2 col-md-9 top-bg">
            <div class="points_sec col-md-12">


                <div class="col-md-3 col-xs-3">
                        <?php if(!empty($details->image)): ?>
                        <img class="img-responsive" src="<?= $details->image; ?>" alt="Recent Post thumbnail"/>
                        <?php else:
                                echo $this->Html->image('banner.png', array('class'=>'img-responsive','alt'=>'Recent Post thumbnail'));
                                endif; ?>
                </div>

                <div class="col-md-9 col-xs-9">

                <h3 class="points_title col-md-12 col-xs-12"> <?php if(!empty($details->title)): echo $details->title; endif; ?> </h3>
                <div class="hidden-xs">
                <div class="col-md-4 col-sm-4 col-xs-4 entry_content_supporter_logos">

                    <p><span> Supported by</span>
                        <a href="#">
                            <?php if(!empty($supporter->image)): ?>
                            <img src="<?= $supporter->image ?>" alt="Supporter Logo" class="detail_top_logo"/>
                            <?php endif; ?>
                        </a>
                    </p>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-4 padding-right-0 padding-left-0 points_">
                    <p class="price"><span> Price </span><span class="ponts_price"><?php if(!empty($details->price)): echo $details->price.'<b>pt</b>'; else : echo "無料"; endif; ?></span></p>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 entry_content_rank">
                    <p>
                        <span> Grade </span>
                        <?php
                        if($details->rank==1)
                            $rank_img = 'modal_logo_1.png';
                        if($details->rank==2)
                            $rank_img = 'modal_logo_2.png';
                        if($details->rank==3)
                            $rank_img = 'modal_logo_3.png';
                        if($details->rank==4)
                            $rank_img = 'modal_logo_4.png';
                        if($details->rank==5)
                            $rank_img = 'modal_logo_5.png';

                        ?>
                        <?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image')); ?>
                    </p>
                </div>
                <!-- /.col-md-2 entry_content_rank -->
                </div>
            </div>

            <div class="col-md-12 col-xs-12 hidden-md hidden-sm hidden-lg">
                <div class="col-md-4 col-sm-4 col-xs-4 entry_content_supporter_logos">

                    <p><span> Supported by</span>
                        <a href="#">
                            <?php if(!empty($supporter->image)): ?>
                            <img src="<?= $supporter->image ?>" alt="Supporter Logo" class="detail_top_logo"/>
                            <?php endif; ?>
                        </a>
                    </p>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-4 padding-right-0 padding-left-0 points_">
                    <p class="price"><span> Price </span><span class="ponts_price"><?php if(!empty($details->price)): echo $details->price.'<b>pt</b>'; else : echo "無料"; endif; ?></span></p>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 entry_content_rank">
                    <p>
                        <span> Grade </span>
                        <?php
                        if($details->rank==1)
                            $rank_img = 'modal_logo_1.png';
                        if($details->rank==2)
                            $rank_img = 'modal_logo_2.png';
                        if($details->rank==3)
                            $rank_img = 'modal_logo_3.png';
                        if($details->rank==4)
                            $rank_img = 'modal_logo_4.png';
                        if($details->rank==5)
                            $rank_img = 'modal_logo_5.png';

                        ?>
                        <?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image')); ?>
                    </p>
                </div>
                <!-- /.col-md-2 entry_content_rank -->
                </div>

            </div>

        </div>
        <div class="col-md-offset-2 col-md-9 input-field-part">

            <!-- <div class="points_table col-xs-offset-1 col-xs-10 hidden-md">
                <div class="points_list clearfix">
                    <div class="pull-left">
                        <p>取得ポイント</p>
                        <p>使用ポイント</p>
                    </div>
                    <div class="pull-right right_ptice">
                        <p id="profile_point" class="profile_point"><//?= $campus_point;?>pt</p>
                        <p class="con_price"><//?php if(!empty($details->price)): echo $details->price.'pt'; else : echo "0pt"; endif; ?></p>
                    </div>
                </div>
                <div class="points_remains clearfix">
                    <div class="pull-left">
                        <p>残高ポイント</p>
                    </div>
                    <div class="pull-right result_point">
                        <p><//?= $campus_point - $details->price;?>pt</p>
                    </div>
                </div>
            </div> -->
            <!-- /.points_table -->

            <div class="col-md-7 col-sm-7 col-xs-12 input-part">
                <div class="col-md-12 email_fld">
                    <label class="hidden-xs">メールアドレス</label>
                    <?php if(!empty($app_profile[0]['email'])){?>
                    <input type="text" value="<?= $app_profile[0]['email']; ?>" name="profile-email" class="profile-email" placeholder="funa@oceanize.co.jp"/>
                    <?php }else{ ?>
                    <input type="text" value="<?= $group_info[0]['email']; ?>" name="profile-email" class="profile-email" placeholder="080-XXXX-XXXX"/>
                    <?php } ?>
                </div>
                <div class="col-md-12 ph_fld">
                    <label class="hidden-xs">電話番号</label>
                    <?php if(!empty($app_profile[0]['phone_number'])){?>
                    <input type="text" value="<?= $app_profile[0]['phone_number']; ?>" name="profile-phone" class="profile-phone" placeholder="funa@oceanize.co.jp"/>
                    <?php }else{ ?>
                    <input type="text" value="<?= $group_info[0]['phone_number']; ?>" name="profile-phone" class="profile-phone" placeholder="080-XXXX-XXXX"/>
                    <?php } ?>
                </div>
            </div>
        <div class="col-md-5 col-sm-5 point-table hidden-xs">
            <div class="points_table _setBorder">
                <div class="points_list clearfix">
                    <div class="pull-left">
                        <p>取得ポイント</p>
                        <p>使用ポイント</p>
                    </div>
                    <div class="pull-right right_ptice">
                        <p id="profile_point" class="profile_point"><?= $campus_point;?>pt</p>
                        <p class="con_price"><?php if(!empty($details->price)): echo $details->price.'pt'; else : echo "0pt"; endif; ?></p>
                    </div>
                </div>
                <!-- /.points_list -->
                <div class="points_remains clearfix">
                    <div class="pull-left">
                        <p>残高ポイント</p>
                    </div>
                    <div class="pull-right result_point">
                        <p><?= $campus_point - $details->price;?>pt</p>
                    </div>
                </div>
                <!-- /.points_remains -->
            </div>
            <!-- /.points_table -->
        </div>
        <!-- /.col-md-7 -->
        </div>
        <input type="hidden" id="entry_url" value="<?php if(!empty($details->id)) { echo $this->Url->build(["controller"=>"Home", "action"=>'entryContent',$details->id]); } ?>" class="hidden"/>
        <input type="hidden" id="content_point" value="<?= $details->price ?>" class="hidden"/>
        <input type="hidden" id="content_title" value="<?= $details->title ?>" class="hidden"/>
        <div class="load_more">
            <p class="text-center">

               <button type="button" class="btn btn-default btn-red btn-big show_confirm_modal not_enough_point"  data-target="<?php echo ($groupName == '') ? '#group_notification' : '#confirm_modal'; ?> ">今すぐ申し込む</button>
            </p>
        </div>
        <!-- /.load_more -->
    </div>
    <!-- /.row -->
</div>
<!-- /.row main_home -->
    <?php
    $this->start('popup');
    echo $this->element('popup/confirm');
            echo $this->element('popup/thanks');
    $this->end();

    ?>
