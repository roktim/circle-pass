<style>
	.main_content{
		width: 100%;
	}
</style>
</div>
</div>


<section class="top_title">

	<div class="container">
		<?php echo $this->Html->image("student_group/title.png")?>
	</div>

</section>
<div class="container">
<section class="second_title">
	<div class="container">
		<div>
			<h1 class="std_group_name"><?= $student_group_name; ?></h1>
		</div>
		<div>
			<?php echo $this->Html->image("student_group/second_sec_sub_title.png",['class' => 'second_subtitle_img'])?>
		</div>

	</div>
</section>


<section class="student_group_form hidden-xs">
	<form class="student-edit-profile" id="student-edit-profile" action="#">

		<div class="col-md-12 col-sm-12 col-lg-12 file-part first-part">
				<label class="col-md-1 text-left">トップ画像</label>
				<div class="col-md-11 col-sm-11 col-lg-11">
					<div class="top-banner-sp">
						<?php if($student_edit['image']==''){  ?>
							<div id="targetLayer"><?php echo $this->Html->image('group_img/banner-sp-student-profile-edit.jpg',['class' => 'banner_img_sp'])?></div>
						<?php }else{ ?>
							<div id="targetLayer"><?php echo $this->Html->image('group_img/'.$student_edit['image'],['class' => 'banner_img_sp'])?></div>
						<?php } ?>
						<input name="userImage" id="userImage" type="file" class="inputFile" />
						<label for="" class="col-xs-12">
							<?php echo $this->Html->image('change-icon.jpg',['class' => 'change-icon file_up_pc'])?>
						</label>
					</div>
				</div>
		</div>

		<div class="col-md-12 col-sm-12 col-lg-12">
		    <label class="col-md-1 text-left">団体構成</label>
			<div class="col-md-11 col-sm-11 col-lg-11">

			<?php if(count($student_gr_members)): foreach ($student_gr_members as $std_mem): ?>
			  <div class="col-md-2 col-sm-2 col-lg-2 first">
			  <label class="col-md-12 col-sm-12 col-lg-12 text-center"><?php echo $std_mem['position']; ?></label>
			  <div class="col-md-12 col-sm-12 col-lg-12"><?php if(!empty($std_mem['image'])) {  echo $this->Html->image($std_mem['image'],['class' => 'avator_large_img']); }?></div>
			  </div>
			<?php endforeach; endif;  ?>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-lg-12 custom-4">
			  <label class="col-md-1 text-left">紹介文</label>
			  <div class="col-md-11"><textarea id="description" class="form-control field2"><?php echo $student_edit['description']; ?></textarea></div>
		</div>
		<div class="col-md-12 col-sm-12 col-lg-12">
			  <label class="col-md-1 col-sm-1 col-lg-1 text-left">人数</label>
			  <div class="col-md-2 col-sm-2 col-lg-2"><input type="text" value="<?php echo $student_edit['member_number']; ?>" id="member_number" class="form-control"></div>
		</div>
		<div class="col-md-12 col-sm-12 col-lg-12">
			<div class="col-md-12 col-sm-12 col-lg-12">
			<div class="col-md-12 col-sm-12 col-lg-12">
			  <label class="col-md-1 col-sm-1 col-lg-1 text-left">活動場所</label>
			  <div class="col-md-5 col-sm-5 col-lg-5"><input type="text" id="location" value="<?php echo $student_edit['location']; ?>" class="form-control"></div>
			</div>
			<div class="col-md-12 col-sm-12 col-lg-12">
			  <label class="col-md-1 col-sm-1 col-lg-1 text-left">こだわり</label>
			  <div class="col-md-5 col-sm-5 col-lg-5">
			  	 <select id="form_name" class="form-control">
			  	 <?php foreach($forms as $fm): ?>
		            <option value="<?php echo $fm['id'];?>" <?php if($student_edit['form_id']== $fm['id']){echo 'selected="selected"';} ?>><?php echo $fm['name']; ?></option>
		        <?php endforeach; ?>
          		</select>

			  </div>
			</div>
			<div class="col-md-12 col-sm-12 col-lg-12">
			  <label class="col-md-1 col-sm-1 col-lg-1 text-left">団体形態</label>
			  <div class="col-md-5 col-sm-5 col-lg-5">

				<select id="type_name" name="type_name" class="form-control">
			  	 <?php foreach($types as $typ): ?>
		            <option value="<?php echo $typ['id'];?>" <?php if($student_edit['type_id']== $typ['id']){echo 'selected="selected"';} ?>><?php echo $typ['name']; ?></option>
		        <?php endforeach; ?>
          		</select>
			</div>
			</div>
			</div>

		</div>
		<div class="col-md-12 col-sm-12 col-lg-12">
			<label class="col-md-1 text-left">カテゴリ</label>
			<div class="col-md-11 col-sm-11 col-lg-11">


			<?php
			$arr=array();
			foreach ($cat_by_user as $key => $value) {
				array_push($arr,$value['student_group_category_id']);
			}

			?>
			<?php foreach ($cat as $category): ?>

			<div class="col-md-2 col-sm-2 col-lg-2">

				<div class="col-md-12 col-sm-12 col-lg-12">
					<input type="checkbox" class="checkbox" id="category_id"  value="<?php echo $category['id']; ?>" <?php
					if (in_array($category['id'], $arr)){
						echo 'checked';
					}
					?>
					>
					<?php echo $category['name']; ?>
			</div>
			</div>
			<?php endforeach; ?>
			</div>

		</div>
		<div class="col-md-12 col-sm-12 col-lg-12">
			<div class="col-md-5 col-sm-5 col-lg-5 bottom-part">
				<div class="col-md-12 col-sm-12 col-lg-12">
					<label class="col-md-2 col-sm-2 col-lg-2 text-left label-part">電話番号</label>
			  		<div class="col-md-10 col-sm-10 col-lg-10"><input type="text" value="<?php echo $student_edit['phone_number']; ?>" id="phone_number" class="form-control number"></div>
				</div>
				<div class="col-md-12 col-sm-12 col-lg-12">
					<label class="col-md-2 col-sm-2 col-lg-2 text-left">Facebook <br/>URL</label>
			  		<div class="col-md-10 col-sm-10 col-lg-10"><input type="url" value="<?php echo $student_edit['facebook_url']; ?>" id="facebook_url" class="form-control url"></div>
				</div>
				<div class="col-md-12 col-sm-12 col-lg-12">
					<label class="col-md-2 col-sm-2 col-lg-2 text-left label-part">Line Name</label>
			  		<div class="col-md-10 col-sm-10 col-lg-10"><input type="text" value="<?php echo $student_edit['line_name']; ?>" id="line_name" class="form-control"></div>
				</div>
			</div>
			<div class="col-md-offset-2 col-md-5 col-sm-5 col-lg-5 bottom-part">
				<div class="col-md-12 col-sm-12 col-lg-12">
					<label class="col-md-2 col-sm-2 col-lg-2 text-left">メール<br/>アドレス</label>
			  		<div class="col-md-10 col-sm-10 col-lg-10"><input type="email" id="email" value="<?php echo $student_edit['email']; ?>" class="form-control"></div>
				</div>
				<div class="col-md-12 col-sm-12 col-lg-12">
					<label class="col-md-2 col-sm-2 col-lg-2 text-left">Twitter<br/>URL</label>
			  		<div class="col-md-10 col-sm-10 col-lg-10"><input type="url" id="twitter_url" value="<?php echo $student_edit['twitter_url']; ?>" class="form-control url"></div>
				</div>
				<div class="col-md-12 col-sm-12 col-lg-12">
					<label class="col-md-2 col-sm-2 col-lg-2 text-left">公式サイト<br/>URL</label>
			  		<div class="col-md-10 col-sm-10 col-lg-10"><input type="url" id="official_url" value="<?php echo $student_edit['official_url']; ?>" class="form-control url"></div>
				</div>
			</div>
		</div>

			<div class="col-md-12 col-sm-12 col-lg-12 submit-button">
				<center><input type="button" id="submit-form1" class="form-control edit-form-submit" value="内容を更新する"></center>
			</div>

			<div class="modal_shadow" id="confirmation" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body edit-student-profile">
							<div class="col-md-12 col-sm-12 col-lg-12">
								<div class="modal_header"><span class="close_modal"> &times; </span></div>
							</div>
							<div class="col-md-12 col-sm-12 col-lg-12">
								<div class="col-md-offset-2 col-md-8  content-part">
									<div class="col-md-11 col-sm-11 col-lg-11">
									<h4 class="confirm-text-lp">内容を更新しました</h4>
									<div class="col-md-offset-1 col-md-6">
									<a href="<?= $this->Url->build('/')  ?>" class="home-redirect">サイトTOP</a></div>
									<div class="close_modal close1">編集に戻る</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</form>
	</section>

<section class="student_group_form_sp hidden-md hidden-sm hidden-lg">
	<form class="student-edit-profile" id="student-edit-profile" action="#">

	<div class="top-banner-sp">
		<?php if($student_edit['image']==''){  ?>
		<div id="targetLayer_sp"><?php echo $this->Html->image('group_img/banner-sp-student-profile-edit.jpg',['class' => 'banner_img_sp'])?></div>
		<?php }else{ ?>
		<div id="targetLayer_sp"><?php echo $this->Html->image('group_img/'.$student_edit['image'],['class' => 'banner_img_sp'])?></div>
		<?php } ?>
		<input name="userImage_sp" id="userImage_sp" type="file" class="inputFile_sp" />
		<label for="" class="col-xs-12">
       		 <?php echo $this->Html->image('change-icon.jpg',['class' => 'change-icon file_up_sp'])?>
    	</label>
	</div>


	<div class="student-edit-profile-sp" action="#">
		<div class="container-part-sp">
			<label class="col-xs-12 text-left">団体構成</label>
			<div class="col-xs-12">
			  <?php if(count($student_gr_members) > 0): foreach ($student_gr_members as $std_mem): ?>
			  <div class="col-xs-3">
			  <label class="col-xs-8 text-center"><?php echo $std_mem['position']; ?></label>
			  <div class="col-xs-12"><?php if(!empty($std_mem['image'])) {  echo $this->Html->image($std_mem['image'],['class' => 'avator_large_img']); }?></div>
			  </div>
			<?php endforeach; endif; ?>
			</div>

		</div>
		<div class="col-xs-12 field-sp">
		<div class="container-part-sp">
			  <label class="col-xs-12 text-left">紹介文</label>
			  <div class="col-xs-12"><textarea id="description_sp" placeholder="入力してください" class="form-control field2"><?php echo $student_edit['description']; ?></textarea></div>
		</div>
		</div>
		<div class="col-xs-12">
			<div class="col-xs-12 field-sp">
			<div class="container-part-sp">
			 <label class="col-xs-4 text-left">人数</label>
			  <div class="col-xs-8"><input type="text" placeholder="数字を入力してください" value="<?php echo $student_edit['member_number']; ?>" id="member_number_sp" class="form-control"></div>
			 </div>
			 </div>
			<div class="col-xs-12 field-sp">
			<div class="container-part-sp">
			  <label class="col-xs-4 text-left">活動場所</label>
			  <div class="col-xs-8">
			  	<input type="text" id="location_sp" value="<?php echo $student_edit['location']; ?>" class="form-control">
			  </div>
			</div>
			</div>
			<div class="col-xs-12 field-sp">
			<div class="container-part-sp">
			  <label class="col-xs-4 text-left">こだわり</label>
			  <div class="col-xs-8">
			  	<select id="form_name_sp" class="form-control">
			  		<option value="">選択してください</option>
			  	 <?php foreach($forms as $fm): ?>
		            <option value="<?php echo $fm['id'];?>" <?php if($student_edit['form_id']== $fm['id']){echo 'selected="selected"';} ?>><?php echo $fm['name']; ?></option>
		        <?php endforeach; ?>
          		</select>
			  </div>
			</div>
			</div>
			<div class="col-xs-12 field-sp last-part">
			<div class="container-part-sp">
			  <label class="col-xs-4 text-left">団体形態</label>
			  <div class="col-xs-8">
			  	<select id="type_name_sp"  class="form-control">
    				<option value="">選択してください</option>
			  	 <?php foreach($types as $typ): ?>
		            <option value="<?php echo $typ['id'];?>" <?php if($student_edit['type_id']== $typ['id']){echo 'selected="selected"';} ?>><?php echo $typ['name']; ?></option>
		        <?php endforeach; ?>
          		</select>
    			</div>
			</div>
			</div>
		</div>
		<div class="container-part-sp">
			<label class="col-xs-12 text-left checkbox-title">カテゴリ</label>

		<div class="col-xs-12 checkbox-text">


			<?php
			$arr=array();
			foreach ($cat_by_user as $key => $value) {
				array_push($arr,$value['student_group_category_id']);
			}

			?>
			<?php foreach ($cat as $category): ?>

			<div class="col-xs-4 checkbox-text">

				<div class="col-xs-12">
					<input type="checkbox" class="checkbox_sp" id="category_id_sp"  value="<?php echo $category['id']; ?>" <?php
					if (in_array($category['id'], $arr)){
						echo 'checked';
					}
					?>
					>
					<?php echo $category['name']; ?>
			</div>
			</div>
			<?php endforeach; ?>
			</div>


		</div>

		<div class="col-xs-12">

				<div class="col-xs-12 field-sp">
				<div class="container-part-sp">
					<label class="col-xs-5 text-left label-part">電話番号</label>
			  		<div class="col-xs-7"><input type="text" placeholder="入力してください" value="<?php echo $student_edit['phone_number']; ?>" id="phone_number_sp" class="form-control"></div>
			  	</div>
				</div>

				<div class="col-xs-12 field-sp">
				<div class="container-part-sp">
					<label class="col-xs-5 text-left">メールアドレス</label>
			  		<div class="col-xs-7"><input type="text" placeholder="入力してください" id="email_sp" value="<?php echo $student_edit['email']; ?>" class="form-control"></div>
			  	</div>
				</div>


				<div class="col-xs-12 field-sp">
				<div class="container-part-sp">
					<label class="col-xs-5 text-left">Facebook URL</label>
			  		<div class="col-xs-7"><input type="text" placeholder="入力してください" value="<?php echo $student_edit['facebook_url']; ?>" id="facebook_url_sp" class="form-control"></div>
			  		</div>
				</div>

				<div class="col-xs-12 field-sp">
					<div class="container-part-sp">
					<label class="col-xs-5 text-left">Twitter URL</label>
			  		<div class="col-xs-7"><input type="text" placeholder="入力してください" id="twitter_url_sp" value="<?php echo $student_edit['twitter_url']; ?>" class="form-control">
			  		</div>
			  		</div>
				</div>

				<div class="col-xs-12 field-sp">
				<div class="container-part-sp">
					<label class="col-xs-5 text-left label-part">Line Name</label>
			  		<div class="col-xs-7"><input type="text" placeholder="入力してください" value="<?php echo $student_edit['line_name']; ?>" id="line_name_sp" class="form-control"></div>
			  		</div>
				</div>

				<div class="col-xs-12 field-sp last-part">
				<div class="container-part-sp">
					<label class="col-xs-5 text-left">公式サイト URL</label>
			  		<div class="col-xs-7"><input type="text" placeholder="入力してください" id="official_url_sp" value="<?php echo $student_edit['official_url']; ?>" class="form-control"></div>
			  		</div>
				</div>
		</div>

		<div class="col-xs-12 submit-button">
		<div class="container-part-sp">
			<center><center><input type="button" id="submit-form1" class="form-control edit-form-submit-sp" value="内容を更新する"></center>
			</div>
		</div>

		<div class="modal_shadow" id="confirmation_sp" role="dialog">
	     <div class="modal-dialog container-part-sp">
	     <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-body edit-student-profile">
			  <div class="col-xs-12">
	        	<div class="modal_header"><span class="close_modal"> &times; </span></div>
	           </div>
	           <div class="col-xs-12 text-center">
		           <h4 class="confirm-text-sp">内容を更新しました</h4>
		           <div class="col-xs-6 mg-less"><div class="close_modal close1">編集に戻る</div></div>
		           <div class="col-xs-6"><a href="" class="home-redirect">サイトTOP</a></div>
	        </div>
	      </div>
	    </div>
	  </div>
	  </div>
</div>
</form>
</section>



<script>
	// $(document).ready(function(){
	// 	var top_height=$('.topbar_wp').outerHeight();
	// 	$('.top_title').css({
	// 	    'margin-top': top_height-10
	// 	});
	// });
</script>
