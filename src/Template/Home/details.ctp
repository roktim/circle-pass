<?php
$this->start('banner');
echo $this->element('banner_details');
$this->end();

$this->start('navmenu');
echo $this->element('navbar/details');
$this->end();
?>
<div class="row main_home home_details">

    <div class="points_sec">
        <p class="point_label"> こんなサークルにおすすめ:  <?php if(!empty($details->recommend_text)): echo $details->recommend_text; endif; ?></p>
    </div>

    <div class="points_sec">
        <p class="point_label"> こんな効果があるかも: <?php if(!empty($details->effect_text)):   echo $details->effect_text; endif; ?></p>
    </div>

    <article class="content">
        <p><?php if(!empty($details->description)): echo $details->description; endif; ?></p>
    </article>
    <div class="row recent_posts post_in_blog">
        <?php if(!empty($details->sub_image_1)): ?>
        <div class="col-md-4 col-sm-4 col-xs-12 post">
            <div class="post_inner">
                <img class="img-responsive" src="<?= $details->sub_image_1; ?>" alt="Sub Image One"/>

                <div class="details clearfix">
                    <p><?php if(!empty($details->sub_description_1)): echo $details->sub_description_1; endif; ?></p>
                </div>
                <!-- /.details -->
            </div>
            <!-- /.post_inner -->
        </div>
        <?php endif; ?>
        <!-- /.col-md-4 -->
        <?php if(!empty($details->sub_image_2)): ?>
        <div class="col-md-4 col-sm-4 col-xs-12 post">
            <div class="post_inner">
                <img class="img-responsive" src="<?= $details->sub_image_2; ?>" alt="Sub Image One"/>
                <div class="details clearfix">
                    <p><?php if(!empty($details->sub_description_2)): echo $details->sub_description_2; endif; ?></p>
                </div>
                <!-- /.details -->
            </div>
            <!-- /.post_inner -->
        </div>
        <?php endif; ?>
        <!-- /.col-md-4 -->
        <?php if(!empty($details->sub_image_3)): ?>
        <div class="col-md-4 col-sm-4 col-xs-12 post">
            <div class="post_inner">
                <img class="img-responsive" src="<?= $details->sub_image_3; ?>" alt="Sub Image One"/>
                <div class="details clearfix">
                    <p><?php if(!empty($details->sub_description_3)): echo $details->sub_description_3; endif; ?></p>
                </div>
                <!-- /.details -->
            </div>
            <!-- /.post_inner -->
        </div>
        <?php endif; ?>
        <!-- /.col-md-4 -->
        <a class="hidden" id="entry_url" href="<?php if(!empty($details->id)) { echo $this->Url->build(["controller"=>"Home", "action"=>'entryContent',$details->id]); } ?>">Entry</a>
        <input type="hidden" id="user_id" value="<?= $user_id; ?>"/>
        <input type="hidden" id="campus_point" value="<?= $campus_point;?>"/>
        <input type="hidden" id="student_group_rate" value="<?= $student_group_rate; ?>"/>
        <input type="hidden" id="permission" value="<?= $permission; ?>"/>
        <input type="hidden" id="content_rank" value="<?php if(!empty($details->rank)): echo $details->rank; else: echo 0; endif; ?>"/>
        <input type="hidden" id="content_point" value="<?php if(!empty($details->price)): echo $details->price; else: echo 0; endif; ?>"/>
        <!-- /.hidden -->

    </div>
    <!-- /.col-md-12 -->
    <div class="load_more row clearfix">
        <p class="text-center"> <button class="btn btn-default btn-red btn-big Go_to_entry">今すぐ申し込む</button></p>
    </div>
    <!-- /.load_more -->
<?php if(count($reviews) > 0): ?>
<div class="row review_content hidden-xs">

    <h3 class="user_circle_title"> 利用サークル </h3>
    <div class="user_circle_main">
        <?php foreach($reviews as $review): ?>
        <div class="row user_circle">
            <div class="col-md-4 col-sm-4 col-xs-12 post_thumb">
                <a href="<?= $this->Url->build(["controller"=>"Home", "action"=>'profile',$review['student_group_id']]) ?>">
                    <?php if($review['student_group']['image']):?>
                    <img class="img-responsive" src="<?= $web_img_base_url.$review['student_group']['image']; ?>" alt="<?= $review['student_group']['name']; ?>"/>
                    <?php else:
                          echo  $this->Html->image('default-thumbnail.jpg',['class' => 'img-responsive','alt'=>'No Image']);
                          endif;
                    ?>
                    </a>
            </div>
            <!-- /.col-md-3 -->
            <div class="col-md-8 col-sm-8 col-xs-12 padding-left-0">
                <div class="user_circle_content">
                <h3 class="post_title"> <?= $review['student_group']['name']; ?> </h3>
                <!-- /.post_title -->
                <article class="details"> <p><?= $review['student_group']['description']; ?></p></article>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.col-md-9 -->
        </div>
        <!-- /.user_circle -->
        <?php endforeach; ?>
    </div>
    <!-- /.user_circle_main -->
    <div class="load_more col-md-12 clearfix">
        <p class="text-center"> <button class="btn btn-default btn-red btn-big Go_to_entry">今すぐ申し込む</button></p>
    </div>
    <!-- /.load_more -->
</div>
<?php endif; ?>
<!-- /.row review_content -->
    <div class="row similar_posts hidden-xs">
        <?php if(count($related_content) > 0):
                foreach($related_content as $related_conetnts):
                ?>

        <div class="col-md-4 col-sm-4 col-xs-12 post related_post">
            <div class="post_item">
                <div class="post_thumb">
                    <?php if(!empty($related_conetnts->tag)): ?>
                    <div class="item_label"> <?=  $related_conetnts->tag;  ?> </div>
                    <?php endif; ?>
                    <!-- /.item_label -->
                    <a href="<?= $this->Url->build(["controller"=>"Home", "action"=>'details',$related_conetnts->id]) ?>">
                        <?php if(!empty($related_conetnts->image)): ?>
                        <img src="<?= $related_conetnts->image; ?>" alt="<?= $related_conetnts->title; ?>"/>
                        <?php else: ?>
                        <?= $this->Html->image('post_1.jpg', array('class'=>'img-responsive','alt'=>'Post Image')); ?>
                        <?php endif; ?>
                    </a>
                    <div class="item_title">
                        <span class="title"><?php if(mb_strlen($related_conetnts->title) > '48'): echo mb_substr($related_conetnts->title, 0, 48)."…"; else: echo $related_conetnts->title;  endif; ?></span>
                        <!-- /.title -->
                        <span class="percentage"> <?= $related_conetnts->tag; ?> </span>
                        <!-- /.percentage -->
                    </div>
                    <!-- /.item_title -->
                </div>
                <!-- /.post_thumb -->
            </div>
            <!-- /.post_item -->
        </div>
        <?php  endforeach; else: ?>
        <div class="col-md-4 col-md-offset-4 post">
            <div class="post_item">
                    <div class="item_title" style="text-align: center">
                        <!-- /.title -->
                        <span class="percentage"> いいえ関連コンテンツ利用可能！</span>
                        <!-- /.percentage -->
                    </div>
                    <!-- /.item_title -->
                <!-- /.post_thumb -->
            </div>
            <!-- /.post_item -->
        </div>
        <?php  endif; ?>
        <?php if(count($related_content) > 3):?>
          <div class="load_more">
              <p class="text-center">
                  <button id="content_loadmore" type="button" class="btn btn-default btn-blue btn-big">もっと見る</button>
              </p>
          </div>
          <?php  endif; ?>
        <!-- /.load_more -->
    </div>
    <!-- /.row -->
</div>
<!-- /.row main_home -->
