<div class="modal_shadow" id="thanks_modal">
    <div id="modal_wrap" class="modal_main">
        <div class="modal_body">
            <div class="modal_row clearfix">
               <div class="thanks_rap">
                   <h3>お申し込みありがとうございました。</h3>
                   <h4>ご登録のメールアドレスに手続き完了のメールを<br/>お送りしました。詳細をご確認ください。</h4>


                   <div class="home_btn">
                       <a href="<?= $this->Url->build('/')  ?>"> <button class="btn btn-default btn-red modal_btn">TOPページに戻る</button></a>
                   </div>
                   <!-- /.home_btn -->

               </div>
               <!-- /.thanks_rap -->
            </div>
            <!-- /.modal_sec -->


        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>
<!-- /.modal_shadow -->
