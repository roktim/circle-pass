<div class="modal_shadow" id="confirm_modal">
    <div id="modal_wrap" class="modal_main">
        <div class="modal_header">  <span class="close_modal"></span></div>
        <!-- /.modal_header -->
        <div class="modal_body">
            <div class="modal_row clearfix">
                <div class="entry_title clearfix">

                    <div class="col-md-3">
                        <?php if(!empty($details->image)): ?>
                        <img class="img-responsive" src="<?= $details->image; ?>" alt="Recent Post thumbnail"/>
                        <?php else:
                                echo $this->Html->image('banner.png', array('class'=>'img-responsive','alt'=>'Recent Post thumbnail'));
                                endif; ?>
                    </div>
                    <!-- /.col-md-4 -->
                    <div class="col-md-9">
                        <div class="points_sec">
                            <h3 class="points_title"> <?php if(!empty($details->title)): echo $details->title; endif; ?> </h3>
                            <div class="col-md-3 col-sm-4 col-xs-12 entry_content_supporter_logos">

                                <p><span> Supported by</span>
                                    <a href="#">
                                        <?php if(!empty($supporter->image)): ?>
                                            <img src="<?= $supporter->image ?>" alt="Supporter Logo" class="detail_top_logo"/>
                                        <?php endif; ?>
                                    </a>
                                </p>
                            </div>

                            <div class="col-md-3 col-sm-4 col-xs-12 padding-right-0 padding-left-0 points_">
                                <p class="price"><span> Price </span><span class="ponts_price"><?php if(!empty($details->price)): echo $details->price.'<b>pt</b>'; else : echo "無料"; endif; ?></span></p>

                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 entry_content_rank">
                                <p>
                                    <span> Grade </span>
                                    <?php
                                    if($details->rank==1)
                                        $rank_img = 'modal_logo_1.png';
                                    if($details->rank==2)
                                        $rank_img = 'modal_logo_2.png';
                                    if($details->rank==3)
                                        $rank_img = 'modal_logo_3.png';
                                    if($details->rank==4)
                                        $rank_img = 'modal_logo_4.png';
                                    if($details->rank==5)
                                        $rank_img = 'modal_logo_5.png';

                                    ?>
                                    <?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image')); ?>
                                </p>
                            </div>
                            <!-- /.col-md-2 entry_content_rank -->

                        </div>

                    </div>
                </div>
                <!-- /.entry_title -->

            </div>
            <!-- /.modal_sec -->
            <div class="modal_row clearfix">

                <div class="col-md-8 col-sm-8 col-xs-5 input-part">
                <div class="col-md-12">
                <label>メールアドレス</label>
                <?php if(!empty($app_profile[0]['email'])){?>
                <span class="pop-up-profile"><?= $app_profile[0]['email']; ?></span>
                <?php }else{ ?>
                <span class="pop-up-profile"><?= $group_info[0]['email']; ?></span>
                <?php } ?>
                </div>
                <div class="col-md-12">
                <label>電話番号</label>
                <?php if(!empty($app_profile[0]['phone_number'])){?>
                <span class="pop-up-profile"><?= $app_profile[0]['phone_number']; ?></span>
                <?php }else{ ?>
                <span class="pop-up-profile"><?= $group_info[0]['phone_number']; ?></span>
                <?php } ?>
                </div>
                </div>
                <div class="pont_popup col-sm-4 col-xs-7 col-md-4">
                    <div class="points_table">
                        <div class="points_list clearfix">
                            <div class="pull-left">
                                <p>取得ポイント</p>
                                <p>使用ポイント</p>
                            </div>
                            <div class="pull-right right_ptice">
                                <p id="profile_point" class="profile_point"><?= $campus_point;?>pt</p>
                                <p class="con_price"><?php if(!empty($details->price)): echo $details->price.'pt'; else : echo "0pt"; endif; ?></p>
                            </div>
                        </div>
                        <!-- /.points_list -->
                        <div class="points_remains clearfix">
                            <div class="pull-left">
                                <p>残高ポイント</p>
                            </div>
                            <div class="pull-right result_point">
                                <p><?= $campus_point - $details->price;?>pt</p>
                            </div>
                        </div>
                        <!-- /.points_remains -->
                    </div>
                </div>
                <!-- /.pont_popup -->
            </div>
            <!-- /.modal_sec -->
            <div class="modal_row clearfix">
                <div class="confirm_btn_rap col-md-6 col-md-offset-3 clearfix">

                    <div class="footer_confirm_btn">
                        <h3>申し込みを確定してよろしいですか？</h3>
                    </div>
                    <!-- /.footer_confirm_btn -->
                    <div class="btn_wrap">
                        <button id="entry_log" class="btn btn-default btn-red modal_btn show_thanks_modal" data-target="thanks_modal">はい</button>
                        <button class="btn btn-default btn-ash close_modal modal_btn">いいえ</button>
                    </div>
                    <!-- /.btn_wrap -->

                </div>
                <!-- /.confirm_btn_rap -->

            </div>
            <!-- /.modal_row -->

        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>
        <!-- /.modal_shadow -->
