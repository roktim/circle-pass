<!-- <div class="row banner hidden-xs"> -->
<div class="row banner">
    <?php if(count($top_images)>1):?>
    <div class="detail_banner_slide col-md-12">
        <?php foreach($top_images as $item ): ?>
        <div class="item">
            <img class="img-responsive" alt="Banner Image" src="<?php echo $item?>" style="max-height:350px" />
        </div>
        <?php endforeach;?>
    </div>
    <!-- /.banner_slide -->
    <?php else:?>
    <div class="col-md-12">
        <div class="item detail_banner" style="background-image: url(<?= str_replace(' ', '%20',$top_images[0]); ?>);">
            <!-- image in style sheet -->
        </div>
        <!-- /.item -->
    </div>
    <?php endif;?>
</div>
<!-- /.row banner -->
