<div class="row details_top">
    <h3 class="title"> <?php if(!empty($details->title)): echo $details->title; endif; ?> </h3>
    <!-- /.title -->
    <div class="price_sec">
        <div class="col-md-3 col-sm-4 col-xs-4 logos">
            <span> Supported By </span>
                 <p>
                     <a href="#">
                         <?php if(!empty($supporter->image)): ?>
                         <img src="<?= $supporter->image ?>" alt="Supporter Logo" class="detail_top_logo"/>
                         <?php endif; ?>
                     </a>
                </p>
             </div>
            <!-- /.logos -->

        <!-- /.col-md-3 -->
    <div class="col-md-3 col-sm-5 col-xs-5 price_ padding-right-0">
        <span> Price </span>
        <h3 class="title"><?php if(!empty($details->price)): echo $details->price.'<b>pt</b>'; else: echo '無料'; endif;?></h3>
        <!-- /.title -->
    </div>
    <!-- /.col-md-5 -->
    <div class="col-md-2 col-sm-3 col-xs-3 details_rank">
        <span> Grade  </span>
        <?php
        if($details->rank==1)
                $rank_img = 'modal_logo_1.png';
        if($details->rank==2)
            $rank_img = 'modal_logo_2.png';
        if($details->rank==3)
            $rank_img = 'modal_logo_3.png';
        if($details->rank==4)
            $rank_img = 'modal_logo_4.png';
        if($details->rank==5)
            $rank_img = 'modal_logo_5.png';
        ?>
        <?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image')); ?>
    </div>
    <!-- /.col-md-5 -->
    <div class="col-md-4 price_btn">
        <p><button class="btn btn-default btn-red Go_to_entry">今すぐ申し込む</button></p>
    </div>
    <!-- /.col-md-4 -->

</div>
    <!-- /.price_sec -->
<div class="price_sec_btn">
    <p><button class="btn btn-default btn-red Go_to_entry">今すぐ申し込む</button></p>
 </div>
<!-- /.price_sec_btn -->
</div>
<!-- /.row -->
