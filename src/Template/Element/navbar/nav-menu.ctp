<div class="row navmenu">
    <div class="col-md-7 col-sm-8 col-xs-8 padding-left-0 search-form-holder">
        <form class="form-inline">
            <div class="form-group">
<!--                1=new Order-->
<!--                2=popularity-->
<!--                3=price low to high-->
<!--                4 =price high to low -->
                <span class="first-search"></span>
                <select class="form-control" id="navMenuList">
                    <option value="0">全ての役割から探す</option>
                    <option value="代表・幹事長">代表・幹事長</option>
                    <option value="企画担当">企画担当</option>
                    <option value="制作・編集担当">制作・編集担当</option>
                    <option value="渉外担当">渉外担当</option>
                    <option value="Web開発担当">Web開発担当</option>
                    <option value="広報担当">広報担当</option>
                    <option value="新歓担当">新歓担当</option>
                </select>
                <!-- /#navMenuList -->
                <span class="second-search"></span>
                <select name="category_diff" class="form-control" id="category_diff">
                    <option value="0">全ての悩みから探す</option>
                    <option value="新規メンバーを募集したい">新規メンバーを募集したい</option>
                    <option value="モチベーションに差がある">モチベーションに差がある</option>
                    <option value="もっと多くのお金を調達したい">もっと多くのお金を調達したい</option>
                    <option value="イベントの告知方法が分からない">イベントの告知方法が分からない</option>
                    <option value="面白い企画を考えられない">面白い企画を考えられない</option>
                    <option value="上手くデザインが作れない">上手くデザインが作れない</option>
                    <option value="上手くHPが作れない">上手くHPが作れない</option>
                    <option value="都合の良い会議スペースが無い">都合の良い会議スペースが無い</option>
                    <option value="安く印刷したい">安く印刷したい</option>
                </select>
                <!-- /#category_diff -->
              </div>

            <button type="submit" class="btn btn-default btn-blue cat_search_btn"> <i class="fa fa-search"></i> </button>
        </form>

    </div>
    <!-- /.col-md-6 -->
    <div class="col-md-5 col-sm-4 col-xs-4 padding-right-0 navmenu_right">
        <div class="top">
            <h3 class="title"> Circlepassとは？  </h3>
            <!-- /.title -->
            <p class="contact_btn"><a href="#" data-target="#review_modal" class="btn btn-default btn-gray show_review_modal btn-review-top">Circlepass <br> とは？</a></p>
        </div>
        <!-- /.top -->
        <div class="bottom">
            <?php echo $this->Html->image('emblem.png', array('class'=>'para-logo','alt'=>'Emblem')) ?>
            <p>大学生をもっと自由にするSmartCampusのサークル版。<br />メンバーのポイント数によって、学生限定の会議室の利用や名刺・チラシの格安印刷、焼肉ディナーなどの限定イベントへの参加権など様々な特典を利用することができます。
            </p>
        </div>
        <!-- /.bottom -->
    </div>
    <!-- /.col-md-6 -->
</div>
<!-- /.row -->
