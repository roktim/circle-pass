<div class="row topbar fixed">
    <div class="topbar_wp">
        <div class="container">

        <div class="col-md-3 col-sm-6 col-xs-5 logo">
            <a href="<?= $this->Url->build('/')  ?>"> <?= $this->Html->image('logo.png', array('class'=>'site_logo','alt'=>'Logo')); ?></a>

        </div>
        <!-- /.col-md-3 -->
        <div class="col-md-9 contact_btn col-sm-6 col-xs-7 main-navbar">
            <?php  if (isset($_COOKIE['sc_user_id'])){ ?>
                <a href="#" class="invite_link invite_friend_modal" data-target="#invite_friend">友達を招待</a>
            <?php } ?>
            <?php $user = "";
                    if (isset($_COOKIE['sc_user_id'])){
                        $user = $_COOKIE['sc_user_id'];
                        $rank_img='';
                        if($student_rank==1)
                            $rank_img = 'modal_logo_1.png';
                        if($student_rank==2)
                            $rank_img = 'modal_logo_2.png';
                        if($student_rank==3)
                            $rank_img = 'modal_logo_3.png';
                        if($student_rank==4)
                            $rank_img = 'modal_logo_4.png';
                        if($student_rank==5)
                            $rank_img = 'modal_logo_5.png';

                    } ?>

                        <ul class="nav navbar-nav navbar-right">
                            <?php if(empty($user)):?>
                            <li class="signup_btn"><a target="_blank" href="<?php echo $web_sso_base_url . '/web/auth?application_id='. $web_sso_application_id .'&client_id='.$web_sso_client_id.'&base_url='.$base_url; ?>">新規登録</a></li>
                            <li class="login_btn"><a class="show_signup_modal" data-target="#signup_modal" href="#">ログイン</a></li>
                            <?php endif;?>

                  <?php if(!empty($user)):?>
                        <?php if($rank_img): ?>
                        <li  class="rank"> <span><?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image')); ?> </span> <?php echo $campuse_point.' Pt'; ?> </li>
                        <?php endif; ?>
                        <li id="logout-btn-pc" class="login_btn">
                            <a class="show_user_info" data-target="#user_info" href="<?php echo $web_sso_base_url . '/web/user/logout'; ?>">
                                <?php if($user_info[0]->icon=='' || empty($user_info[0]->icon)): ?>
                                    <?= $this->Html->image('user-pc.png', array('class'=>'logout_btn_sp img-responsive','alt'=>'Logout')); ?>
                                <?php else: ?>
                                    <img class="img-circle logout_btn_sp img-responsive" src="<?php echo $user_info[0]->icon ?>" alt="User avatar">
                                <?php endif; ?>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(empty($user)):?>
                        <li id="login_icon_sp" class="logout_icon_sp"><a class="show_signup_modal" data-target="#signup_modal" href="#">ログイン</a></li>
                        <?php endif; if(!empty($user)): ?>
                        <li id="logout_icon_sp" class="logout_icon_sp login_icon_sp_d"><a href="#" class="show_user_info" data-userID="<?php echo $user; ?>" data-target="#user_info">
                                <?php if($user_info[0]->icon=='' || empty($user_info[0]->icon)): ?>
                                    <?= $this->Html->image('user.png', array('class'=>'logout_btn_sp img-responsive','alt'=>'Logout')); ?>
                                <?php else: ?>
                                    <img class="img-circle logout_btn_sp img-responsive" src="<?php echo $user_info[0]->icon ?>" alt="User avatar">
                                <?php endif; ?>
                            </a>
                        </li>
                        <?php endif;?>
                      <!-- <li id="visible-lp" class="visible-lp"> <p class="text-right"><a href="http://oceanize.co.jp/contact.php?referer=circle_pass" target="_blank" class="btn btn-default btn-red top-right-contact-btn"> 協賛したい 企業の方はコチラから </a></p></li>-->
                        <?php if(empty($user)):?>
                  <!--      <li id="visible-sp" class="visible-sp"> <p class="text-right"><a href="http://oceanize.co.jp/contact.php?referer=circle_pass" target="_blank" class="btn btn-default btn-red top-right-contact-btn"> 協賛したい<br/>企業の方はコチラから </a></p></li>-->
                        <?php endif;?>
                    </ul>
          <!-- /.rightbtn -->
    </div>
    <!-- /.col-md-9 -->


        </div>
        <!-- /.continer -->
    </div>
    <!-- /.topbar_wp -->
</div>
<!-- /.row -->
