<div class="row details_top hidden-xs">
    <h3 class="title"> <?php if(!empty($details->title)): echo $details->title; endif; ?> </h3>
    <!-- /.title -->
    <div class="price_sec">
        <div class="col-md-3 col-sm-6 col-xs-5 logos">
            <span> Supported by </span>
                 <p>
                     <a href="#">
                         <?php if(!empty($supporter->image)): ?>
                         <img src="<?= $supporter->image ?>" alt="Supporter Logo" class="detail_top_logo"/>
                         <?php endif; ?>
                     </a>
                </p>
             </div>
            <!-- /.logos -->

        <!-- /.col-md-3 -->
    <div class="col-md-5 col-sm-6 col-xs-7 price_">
        <span> Price </span>
        <h3 class="title"><?php if(!empty($details->price)): echo $details->price.'<b>pt</b>'; else: echo '無料'; endif;?></h3>
        <!-- /.title -->
    </div>
    <!-- /.col-md-5 -->
</div>
    <!-- /.price_sec -->
</div>
<!-- /.row -->