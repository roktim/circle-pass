<div class="row banner">
    <div class="sp-review-btn visible-xs">
        <a href="<?= $this->Url->build(["controller"=>"Home", "action"=>'reviewlist']) ?>"> 利用者の声 <i class="fa fa-comment" aria-hidden="true"></i></a>
    </div>
    <!-- /.sp-review-btn -->
   <div class="banner_slide col-md-12">

       <?php if(count($advertises)>0):
           foreach($advertises as $advertise ):
           ?>
       <div class="item">
                   <?php echo $this->Html->image('banner_2.jpg',array('class'=>'img-responsive','alt'=>'Banner Image')) ?>
                   <div class="caption">
                       <div class="caption_inner">
                           <h3 class="item-title">
                               Circlepassは <br> サークル・学生団体を <br>応援します！
                           </h3>



                       </div>
                       <!-- /.caption_inner -->
                   </div>
                   <!-- /.caption -->
               </div>

       <?php endforeach; else: ?>
       <div class="item">
           <?php echo $this->Html->image('banner_2.jpg',array('class'=>'img-responsive','alt'=>'Banner Image')) ?>
               <div class="caption">
                   <div class="caption_inner">
                       <h3 class="item-title">
                           Circlepassは <br> サークル・学生団体を <br>応援します！
                       </h3>
                       <!-- /.item-title -->

                       <!-- /.review -->
                   </div>
                   <!-- /.caption_inner -->
               </div>
               <!-- /.caption -->
       </div>
       <div class="item">
           <?php echo $this->Html->image('second_banner.jpg',array('class'=>'img-responsive','alt'=>'Banner Image')) ?>
           <div class="caption">
               <div class="caption_inner">
                   <h3 class="item-title">
                       Circlepassは <br> サークル・学生団体を <br>応援します！
                   </h3>
                   <!-- /.item-title -->

                   <!-- /.review -->
               </div>
               <!-- /.caption_inner -->
           </div>
           <!-- /.caption -->
       </div>
       <div class="item">
           <?php echo $this->Html->image('third_banner.jpg',array('class'=>'img-responsive','alt'=>'Banner Image')) ?>
           <div class="caption">
               <div class="caption_inner">
                   <h3 class="item-title">
                       Circlepassは <br> サークル・学生団体を <br>応援します！
                   </h3>
                   <!-- /.item-title -->

                   <!-- /.review -->
               </div>
               <!-- /.caption_inner -->
           </div>
           <!-- /.caption -->
       </div>
       <?php endif; ?>
   </div>
   <!-- /.banner_slide -->
</div>
<!-- /.row banner -->
<div class="actual_result_pc">
   <div class="actual_result_img_pc">
       <?php echo $this->Html->image('actual-result-pc.png',array('class'=>'img-responsive','alt'=>'Actual result')) ?>
   </div>
   <!-- /.actual_result_img -->
</div>
<!-- /.actual_result -->
<div class="actual_result_sp">
   <div class="actual_result_img_sp">
       <?php echo $this->Html->image('actual-result-sp.png',array('class'=>'img-responsive','alt'=>'Actual result')) ?>
   </div>
   <!-- /.actual_result_img_sp -->
</div>
<!-- /.actual_result_sp -->
