<?php if(count($contents)>0):
         foreach($contents as $content):
            ?>
            <div class="col-md-4 col-sm-6 col-xs-6 post ajax_post">
                <a href="<?= $this->Url->build(["controller"=>"Home", "action"=>'details',$content->id]) ?>">
                <div class="post_item">
                    <div class="post_thumb">
                        <?php if($content->tag): ?> <div class="item_label"> <?= $content->tag; ?> </div> <?php endif; ?>
                        <!-- /.item_label -->
                        <?php if($content->image): ?>
                        <img class="img-responsive" src="<?= $content->image; ?>" alt="<?= $content->title ?>">
                            <?php else:
                                    echo $this->Html->image('post_2.jpg',array('class'=>'img-responsive','alt'=>'No Image'));
                                    endif; ?>

                        <div class="item_title">
                            <?php if($content->tag) {$subTitleLimit = 50;} else {$subTitleLimit = 60;}  ?>
                            <span class="title"><?php if(mb_strlen($content->title,'UTF-8') > $subTitleLimit): echo mb_substr($content->title, 0, $subTitleLimit,'UTF-8')."…"; else: echo $content->title;  endif; ?></span>
                            <!-- /.title -->
                            <?php if($content->tag): ?> <span class="percentage"> <?= $content->tag; ?> </span> <?php endif; ?>
                            <!-- /.percentage -->
                        </div>
                        <!-- /.item_title -->
                    </div>
                    <!-- /.post_thumb -->
                    <div class="post_desc">
                        <div class="item_title">
                            <?php if($content->tag) {$subTitleLimit = 10;} else {$subTitleLimit = 10;}  ?>
                            <span class="title"><?php if(mb_strlen($content->title,'UTF-8') > $subTitleLimit): echo mb_substr($content->title, 0, $subTitleLimit,'UTF-8')."…"; else: echo $content->title;  endif; ?></span>
                            <!-- /.title -->
                            <?php if($content->tag): ?> <span class="percentage"> <?= $content->tag; ?> </span> <?php endif; ?>
                            <!-- /.percentage -->
                        </div>
                        <div class="post_content"><?php if(mb_strlen($content->description,'UTF-8') > 36): echo mb_substr($content->description, 0, 36,'UTF-8')."…"; else: echo $content->description;  endif; ?></div>
                        <div class="post_action">
                            <div class="col-md-3 col-sm-3 col-xs-3 no-padding-sp padding-left-0 padding-right-0">
                                <div class="con_sup_img">
                                    <?php if($content['supporter']['image']): ?>
                                        <img src="<?= $content['supporter']['image']; ?>" alt="<?= $content['supporter']['name']; ?>">
                                    <?php else:
                                        echo "企業ロゴ";
                                    endif; ?>
                                </div>
                                <!-- /.button -->
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-7 no_pad_left no_pad_right">
                                <div class="support_by">
                                    <p> supported by </p>
                                    <h4 class="sup_pc"><?= $content['supporter']['name']; ?></h4>
                                    <h4 class="sup_sp"><?= mb_substr($content['supporter']['name'],0,5)."..."; ?></h4>

                                </div>
                                <!-- /.support_by -->
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 no_pad_left no_pad_right post_rank">
                                <?php
                                if($content['rank']==1)
                                    $rank_img = 'modal_lg_logo_1.png';
                                if($content['rank']==2)
                                    $rank_img = 'modal_lg_logo_2.png';
                                if($content['rank']==3)
                                    $content['rank'] = 'modal_lg_logo_3.png';
                                if($content['rank']==4)
                                    $rank_img = 'modal_lg_logo_4.png';
                                if($content['rank']==5)
                                    $rank_img = 'modal_lg_logo_5.png';

                                ?>
                                <?php echo $this->Html->image($rank_img, array('class'=>'rank','alt'=>'Rank Image')); ?>
                            </div>
                            <!-- /.col-md-2 col-sm-2 col-xs-2 no_pad_left no_pad_right -->

                        </div>
                        <!-- /.post_action -->
                    </div>
                    <!-- /.post_desc -->
                </div>
                <!-- /.post_item -->
                </a>
            </div>
        <?php endforeach; else: ?>
        <div class="col-md-12 col-xs-12 post ajax_post empty_posts_container" >
            <h3 class="empty-title text-center"> この条件に一致するコンテンツはまだ登録されていません… </h3>
            <!-- /.empty-title -->
        </div>
        <!-- /.col-md-12 col-xs-12 post ajax_post -->
        <?php endif; ?>
    <?php if($this->Paginator->counter('{{pages}}')>1): ?>

    <div class="col-md-12 load_more_post">
        <div class="main_load">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <?php if($this->Paginator->current() != $this->Paginator->counter('{{pages}}') ):?>
        <?=  $this->Paginator->next('もっと見る', array('data-page'=>1)); ?>
    <?php endif;?>
    </div>
    <!-- /.col-md-12 -->
<?php endif; ?>
