<?php use Cake\Routing\Router;?>
<!DOCTYPE html>
<?php
        /**
        * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
        * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
        *
        * Licensed under The MIT License
        * For full copyright and license information, please see the LICENSE.txt
        * Redistributions of files must retain the above copyright notice.
        *
        * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
        * @link          http://cakephp.org CakePHP(tm) Project
        * @since         0.10.0
        * @license       http://www.opensource.org/licenses/mit-license.php MIT License
        */

        $title = 'Error Page';
        ?>
<html>
<head>
    <?= $this->Html->charset() ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>
        <?= $title ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!--    twitter bootstrap -->
    <?= $this->Html->css('bootstrap/bootstrap.css') ?>
    <?= $this->Html->css('bootstrap/bootstrap-theme.css') ?>
    <?= $this->Html->css('bootstrap-select.min.css') ?>
    <!--    font awesome -->
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css') ?>
    <!--    Owl Carousel Slide-->
    <?= $this->Html->css('owl_carousel/owl.carousel.css') ?>

    <?= $this->Html->css('content.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->meta('description','大学生をもっと自由にするSmartCampusのサークル版。学生団体やサークルの活動をもっと自由に、もっとスマートに。学生限定の会議室やクリエイティブソフトが無料で使えるサポート、さらに学生団体間の交流イベントも開催。'); ?>
    <?= $this->Html->meta('keyword','oceanize, smartcampus, SC, circlepass, 学生団体, サークル, 体育会, 大学生, タダコピ, 印刷, 名刺, 協賛, 焼肉, 無料, 割引, 自由, ポイント, 就活, 引き継ぎ, 起業, 学生起業, 貸会議室, 作業, Adobe'); ?>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-79136532-3', 'auto');
    ga('send', 'pageview');

    </script>

</head>
<body>
<div class="container">
    <?= $this->element('navbar/top-bar'); ?>

    <?php
            if( $this->fetch('banner') ):
            /*
            Banner area
            */
            echo $this->fetch('banner');
            endif;
            ?>
    <?php
    if($this->fetch('reviewheader')){
        /*
        * reviewheader
        */
        echo $this->fetch('reviewheader');
    }
    ?>

    <?= $this->Flash->render() ?>
    <div class="container">
        <?php
                if($this->fetch('navmenu')){
                /*
                * Add the menus
                */
                echo $this->fetch('navmenu');
                }
                ?>


        <?= $this->fetch('content') ?>
    </div>


    <footer class="row footer_main">
        <div class="footer-pc">
            <div class="col-md-2 col-sm-2 col-xs-3 footer_left_logo">
                <p><a href="<?= $this->Url->build('/')  ?>"> <?= $this->Html->image('footer_logo.png', array('alt'=>'Logo')); ?></a></p>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12 footer_right_logo">
                <div class="footer_no">
                    <div class="no">
                        <p> 業界 <br> No.1</p>
                    </div>
                </div>
                <div class="no_text">
                    <p> 学生市場のマーケティング、<br> 学生向けメディアのプランニングは <br> オーシャナイズにお任せください。 </p>
                </div>

                <a href="http://oceanize.co.jp" target="_blank" class="right_logo"> <?=
                    $this->Html->image('footer_right_logo.png'); ?></a>
                <h3 class="footer_title"> Circlepassはオーシャナイズが運営しています。 </h3>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12 footer_slogan">
                <div class="footer-contact">
                    <div id="visible-lp" class="visible-sp">
                        <p class=""><a href="http://oceanize.co.jp/contact.php?referer=circle_pass" target="_blank" class="btn btn-default btn-red top-right-contact-btn footer_con_btn"> 協賛したい 企業の方はコチラから </a></p>
                    </div>

                </div>
                <p class="slogan"> 株式会社オーシャナイズ 〒106-0031 東京都港区西麻布3-22-10 </p>
            </div>
        </div>
        <!-- /.footer-pc -->
        <div class="footer-sp">
           <div class="col-md-6 col-xs-10 footer_slogan">
                <h3 class="footer_title"> Circlepassはオーシャナイズが運営しています。 </h3>
                <p class="slogan"> 株式会社オーシャナイズ 〒106-0031 東京都港区西麻布3-22-10 </p>
            </div>
            <div class="col-md-4 col-xs-12 footer_right_logo">
                <div class="footer_no">
                    <div class="no">
                        <p> 業界 <br> No.1</p>
                    </div>
                </div>
                <div class="no_text">
                    <p> 学生市場のマーケティング、<br> 学生向けメディアのプランニングは <br> オーシャナイズにお任せください。 </p>
                </div>

                <a href="http://oceanize.co.jp" target="_blank" class="right_logo"> <?=
                    $this->Html->image('footer_right_logo.png'); ?></a>
                <div class="footer-contact">
                    <div id="visible-lp" class="visible-sp">
                        <p class=""><a href="http://oceanize.co.jp/contact.php?referer=circle_pass" target="_blank" class="btn btn-default btn-red top-right-contact-btn"> 協賛したい 企業の方はコチラから </a></p>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.footer-sp -->
    </footer>

</div>
</body>
</html>
