<?php use Cake\Routing\Router;?>
<!DOCTYPE html>
<?php
        /**
        * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
        * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
        *
        * Licensed under The MIT License
        * For full copyright and license information, please see the LICENSE.txt
        * Redistributions of files must retain the above copyright notice.
        *
        * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
        * @link          http://cakephp.org CakePHP(tm) Project
        * @since         0.10.0
        * @license       http://www.opensource.org/licenses/mit-license.php MIT License
        */

        $cakeDescription = 'Circle Pass: Here is circle pass description';
        ?>
<html>
<head>
    <?= $this->Html->charset() ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>
        <?= $title; ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!--    twitter bootstrap -->
    <?= $this->Html->css('bootstrap/bootstrap.css') ?>
    <?= $this->Html->css('bootstrap/bootstrap-theme.css') ?>
    <?= $this->Html->css('bootstrap-select.min.css') ?>
    <!--    font awesome -->
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css') ?>
    <!--    Owl Carousel Slide-->
    <?= $this->Html->css('owl_carousel/owl.carousel.css') ?>

    <?= $this->Html->css('content.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->meta('description','大学生をもっと自由にするSmartCampusのサークル版。学生団体やサークルの活動をもっと自由に、もっとスマートに。学生限定の会議室やクリエイティブソフトが無料で使えるサポート、さらに学生団体間の交流イベントも開催。'); ?>
    <?= $this->Html->meta('keyword','oceanize, smartcampus, SC, circlepass, 学生団体, サークル, 体育会, 大学生, タダコピ, 印刷, 名刺, 協賛, 焼肉, 無料, 割引, 自由, ポイント, 就活, 引き継ぎ, 起業, 学生起業, 貸会議室, 作業, Adobe'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-79136532-3', 'auto');
    ga('send', 'pageview');

    </script>

</head>
<body>
<script type="text/javascript">
    var url = '<?php echo $this->Url->build(["controller" => "Home"]) ?>';
    var root = '<?php echo $this->Url->build('/');  ?>';
</script>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<div class="container">
    <?= $this->element('navbar/top-bar'); ?>

    <?php
            if( $this->fetch('banner') ):
            /*
            Banner area
            */
            echo $this->fetch('banner');
            endif;
            ?>
    <?php
    if($this->fetch('reviewheader')){
        /*
        * reviewheader
        */
        echo $this->fetch('reviewheader');
    }
    ?>

    <?= $this->Flash->render() ?>
    <div class="main_content">
        <?php
                if($this->fetch('navmenu')){
                /*
                * Add the menus
                */
                echo $this->fetch('navmenu');
                }
                ?>


        <?= $this->fetch('content') ?>
    </div>




</div>
<footer class="row footer_main">
<div class="container">
  <div class="row">
    <div class="col-md-12">
    <div class="footer-pc">
        <div class="col-md-3 col-sm-3 col-xs-3 footer_left_logo">
            <p><a href="<?= $this->Url->build('/')  ?>"> <?= $this->Html->image('footer_logo.png', array('alt'=>'Logo')); ?></a></p>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 footer_right_logo">
          <div class="footer_mid_content">

            <div class="no_logo">
              <div class="footer_no"><div class="no"><p> 業界 <br> No.1</p></div></div>
              <div class="no_text">
                  <p> 学生市場のマーケティング、<br> 学生向けメディアのプランニングは <br> オーシャナイズにお任せください。 </p>
              </div>
              <a href="http://oceanize.co.jp" target="_blank" class="right_logo"> <?=
                  $this->Html->image('footer_right_logo.png'); ?></a>
            </div>
            <h3 class="footer_title"> Circlepassはオーシャナイズが運営しています。 </h3>

          </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 footer_slogan">
            <div class="footer-contact">
                <div id="visible-lp" class="visible-sp">
                    <p class=""><a href="http://oceanize.co.jp/contact.php?referer=circle_pass" target="_blank" class="btn btn-default btn-red top-right-contact-btn footer_con_btn"> 協賛したい 企業の方はコチラから </a></p>
                </div>

            </div>
            <p class="slogan"> 株式会社オーシャナイズ 〒106-0031 東京都港区西麻布3-22-10 </p>
        </div>
    </div>
    <!-- /.footer-pc -->
    <div class="footer-sp">
       <div class="col-md-6 col-xs-12 footer_slogan">

         <div class="footer_caption">
           <div class="txt_cap">
             <h3 class="footer_title"> Circlepassはオーシャナイズが運営しています。 </h3>
             <p class="slogan"> 株式会社オーシャナイズ 〒106-0031 東京都港区西麻布3-22-10 </p>
           </div>
           <div class="oc-logo">
             <a href="http://oceanize.co.jp" target="_blank" class="right_logo sp_oc_logo"> <?=
              $this->Html->image('footer_right_logo.png'); ?></a>
           </div>
         </div>

        </div>
        <div class="col-md-4 col-xs-12 footer_right_logo">
            <div class="footer_no">
                <div class="no">
                    <p> 業界 <br> No.1</p>
                </div>
            </div>
            <div class="no_text">
                <p> 学生市場のマーケティング、<br> 学生向けメディアのプランニングは <br> オーシャナイズにお任せください。 </p>
            </div>


            <div class="footer-contact">
                <div id="visible-lp" class="visible-sp">
                    <p class=""><a href="http://oceanize.co.jp/contact.php?referer=circle_pass" target="_blank" class="btn btn-default btn-red top-right-contact-btn"> 協賛したい 企業の方はコチラから </a></p>
                </div>

            </div>
        </div>
    </div>
    <!-- /.footer-sp -->
    </div>
  </div>
  </div>
</footer>



<?php
        if( $this->fetch('popup') ):
        /*
        popup area
        */
        echo $this->fetch('popup');
        endif;
        ?>
<!-- /.container -->
<div class="modal_shadow" id="review_modal">
    <div class="modal_main">
        <div class="modal_header"><span class="close_modal">  </span></div>
        <!-- /.modal_header -->
        <div class="modal_body">
            <div class="modal_sec">
                <h3 class="title"> Circlepassって何？ </h3>
                <!-- /.title -->
                <p class="sub_title sub_title_first">サークル・学生団体として登録しログインすればメンバーの合計Cポイントに
                    応じたサービスが利用できたり、特典がもらえる認定制度です。
                </p>
                <!-- /.sub_title -->
            </div>
            <!-- /.modal_sec -->
            <div class="modal_sec less_border_sec">
                <h3 class="title logo_title">Circlepassにはグレードが5つあります</h3>
                <p class="grade_title">団体でのポイント数に応じてグレードがUPするので、ポイントを貯めるほど
                    お得なサービスが受けれます。</p>
                <!-- /.grade_title -->
                <!-- /.title -->
                <ul class="modal_logos tab_lists">
                    <li class="active" data-target="#tab1"><?= $this->Html->image('modal_logo_1.png',array('alt'=>'Modal Logo 1')) ?> <!--<p> お得度：小</p>--></li>
                    <li data-target="#tab2"><?= $this->Html->image('modal_logo_2.png',array('alt'=>'Modal Logo 2')) ?></li>
                    <li data-target="#tab3"><?= $this->Html->image('modal_logo_3.png',array('alt'=>'Modal Logo 3')) ?></li>
                    <li data-target="#tab4"><?= $this->Html->image('modal_logo_4.png',array('alt'=>'Modal Logo 4')) ?></li>
                    <li data-target="#tab5"><?= $this->Html->image('modal_logo_5.png',array('alt'=>'Modal Logo 5')) ?> <!--<p> お得度：大</p>--></li>
                </ul>
                <div class="grade-arrow">
                    <?= $this->Html->image('grade_arrow.png',array('alt'=>'About Arrow')); ?>
                </div>
                <!-- /.grade-arrow -->
            </div>
            <!-- /.modal_sec -->
            <div class="modal_sec">
                <div class="tabs-content">
                    <div class="tab active" id="tab1">
                        <div class="pre_next_tab">
                            <ul>
                                <li class="tabPrev"><?= $this->Html->image('select_grade_arrow_left.png',array('alt'=>'pev img')); ?></li>
                                <li class="next_pev_txt">&nbsp;&nbsp;ブルー pass&nbsp;</li>
                                <li class="tabNext"><?= $this->Html->image('select_grade_arrow_right.png',array('alt'=>'pev img')); ?></li>
                            </ul>
                        </div>
                        <!-- /.pre_next_tab -->
                        <!-- /.title -->
                        <div class="modal_sub_sec">
                            <div class="modal_lg_logo">
                                <p><?= $this->Html->image('modal_lg_logo_1.png') ?></p>
                            </div>
                            <!-- /.modal_lg_logo -->
                            <div class="modal_list">
                                <ul>
                                    <li><a href="#">例１：名刺やフリーペーパーの印刷等の制作相談会に参加可能！</a></li>
                                    <li><a href="#">例２：渉外の資料作成のアドバイスやプロの資料を閲覧可能！</a></li>
                                    <li><a href="#">例３：飲み会の割引券プレゼント！</a></li>
                                </ul>
                             </div>
                            <!-- /.modal_list -->
                        </div>
                        <!-- /.modal_sub_sec -->
                    </div>
                    <!-- /.tab1 -->
                    <div class="tab" id="tab2">
                        <div class="pre_next_tab">
                            <ul>
                                <li class="tabPrev"><?= $this->Html->image('select_grade_arrow_left.png',array('alt'=>'pev img')); ?></li>
                                <li class="next_pev_txt">&nbsp;&nbsp;レッド pass&nbsp;</li>
                                <li class="tabNext"><?= $this->Html->image('select_grade_arrow_right.png',array('alt'=>'pev img')); ?></li>
                            </ul>
                        </div>
                        <!-- /.pre_next_tab -->
                        <!-- /.title -->
                        <div class="modal_sub_sec">
                            <div class="modal_lg_logo">
                                <p><?= $this->Html->image('modal_lg_logo_2.png') ?></p>
                            </div>
                            <!-- /.modal_lg_logo -->
                            <div class="modal_list">
                                <ul>
                                    <li><a href="#">例１：学生団体運営における必須ノウハウ事例集プレゼント＆セミナー受講可能！</a></li>
                                    <li><a href="#">例２：タダコピアプリなどの学生向けメディアでみなさんの活動を広報！</a></li>
                                    <li><a href="#">例３：新橋の学生向け会議室の利用が可能！</a></li>
                                </ul>
                             </div>
                            <!-- /.modal_list -->
                        </div>
                        <!-- /.modal_sub_sec -->
                    </div>
                    <!-- /.tab2 -->
                    <div class="tab" id="tab3">
                        <div class="pre_next_tab">
                            <ul>
                                <li class="tabPrev"><?= $this->Html->image('select_grade_arrow_left.png',array('alt'=>'pev img')); ?></li>
                                <li class="next_pev_txt">シルバー pass</li>
                                <li class="tabNext"><?= $this->Html->image('select_grade_arrow_right.png',array('alt'=>'pev img')); ?></li>
                            </ul>
                        </div>
                        <!-- /.pre_next_tab -->
                        <!-- /.title -->
                        <div class="modal_sub_sec">
                            <div class="modal_lg_logo">
                                <p><?= $this->Html->image('modal_lg_logo_3.png') ?></p>
                            </div>
                            <!-- /.modal_lg_logo -->
                            <div class="modal_list">
                                <ul>
                                    <li><a href="#">例１：HPの作成やセンパイからの引き継ぎトラブルに関しての相談会に参加可能！</a></li>
                                    <li><a href="#">例２：フリーペーパーや学祭パンフレットの広告ページ買います！</a></li>
                                    <li><a href="#">例３：タダコピ周辺の学内ポスターなどの学生向けメディアでみなさんの活動を広報！</a></li>
                                </ul>
                             </div>
                            <!-- /.modal_list -->
                        </div>
                        <!-- /.modal_sub_sec -->
                    </div>
                    <!-- /.tab3 -->
                    <div class="tab" id="tab4">
                        <div class="pre_next_tab">
                            <ul>
                                <li class="tabPrev"><?= $this->Html->image('select_grade_arrow_left.png',array('alt'=>'pev img')); ?></li>
                                <li class="next_pev_txt">ゴールド pass</li>
                                <li class="tabNext"><?= $this->Html->image('select_grade_arrow_right.png',array('alt'=>'pev img')); ?></li>
                            </ul>
                        </div>
                        <!-- /.pre_next_tab -->
                        <!-- /.title -->
                        <div class="modal_sub_sec">
                            <div class="modal_lg_logo">
                                <p><?= $this->Html->image('modal_lg_logo_4.png') ?></p>
                            </div>
                            <!-- /.modal_lg_logo -->
                            <div class="modal_list">
                                <ul>
                                    <li><a href="#">例１：渉外シーンなどで使える便利なタブレットをプレゼント！</a></li>
                                    <li><a href="#">例２：タダコピchなどの学生向けメディアでみなさんの活動を広報！</a></li>
                                    <li><a href="#">例３：フリーペーパーや学祭パンフレットの広告ページ買います！</a></li>
                                </ul>
                             </div>
                            <!-- /.modal_list -->
                        </div>
                        <!-- /.modal_sub_sec -->
                    </div>
                    <!-- /.tab4 -->
                    <div class="tab" id="tab5">
                        <!-- /.title -->
                        <div class="pre_next_tab">
                            <ul>
                                <li class="tabPrev"><?= $this->Html->image('select_grade_arrow_left.png',array('alt'=>'pev img')); ?></li>
                                <li class="next_pev_txt">ブラック pass</li>
                                <li class="tabNext"><?= $this->Html->image('select_grade_arrow_right.png',array('alt'=>'pev img')); ?></li>
                            </ul>
                        </div>
                        <!-- /.pre_next_tab -->
                        <div class="modal_sub_sec">
                            <div class="modal_lg_logo">
                                <p><?= $this->Html->image('modal_lg_logo_5.png') ?></p>
                            </div>
                            <!-- /.modal_lg_logo -->
                            <div class="modal_list">
                                <ul>
                                    <li><a href="#">例１：MacにデザインソフトのAdobeソフト（AI・PSD）を入れてプレゼント！</a></li>
                                    <li><a href="#">例２：タダコピやその他の学生向けメディアでみなさんの活動を広報！</a></li>
                                    <li><a href="#">例３：起業経験者が学生の起業をサポート！</a></li>
                                </ul>
                            </div>
                            <!-- /.modal_list -->
                        </div>
                        <!-- /.modal_sub_sec -->
                    </div>
                    <!-- /.tab5 -->
                </div>
                <!-- /.tabs-content -->
            </div>
            <!-- /.modal_sec -->
            <div class="modal_sec">
                <h3 class="title bottom_logo"> Cポイントはどうやって貯めるの？</h3>
                <!-- /.title -->
                <div class="client_logos">
                    <ul>
                        <li class="client_one"><a href="#"><?= $this->Html->image('client_1.png',array('alt'=>'Client One')); ?> </a></li>
                        <li class="client_two"><a href="#"><?= $this->Html->image('client_2.png',array('alt'=>'Client Two')); ?> </a></li>
                        <li class="client_three"><?= $this->Html->image('client_3.png',array('alt'=>'Client Three')); ?></li>
                        <li class="client_four"><?= $this->Html->image('client_5.png',array('alt'=>'Client Four')); ?></li>
                        <li class="client_five"><?= $this->Html->image('client_6.png',array('alt'=>'Client Five')); ?></li>
                    </ul>
                </div>
                <!-- /.client_logos -->
                <p class="sub_title sub_title_bottom">
                    上記のアプリ内、Campus Workでミッションをクリアしたり
                    イベントに参加することでCポイントを獲得できます。
                </p>
                <!-- /.sub_title -->
            </div>
            <!-- /.modal_sec -->
            <div class="modal_sec">
                <div class="about_grade_img">
                    <?= $this->Html->image('about_grade.png',array('alt'=>'About Grade')); ?>
                </div>
                <!-- /.about_grade_img -->
            </div>
            <!-- /.modal_sec -->
        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>
<!-- /.modal_shadow -->
<!--signup-->
<div class="modal_shadow" id="signup_modal">
    <div id="signup_modal_wrap" class="modal_main">
        <div class="modal_header"><span class="close_modal"> </span></div>
        <!-- /.modal_header -->
        <div class="modal_body">
            <div class="modal_row clearfix">
                <div class="auth_msg">
                    <h4 id="alert_msg"></h4>
                </div>
                <!-- /.auth_msg -->
                <div class="col-md-6 ">
                    <div class="login_by_social">
                        <a href="#">
                            <h4>ソーシャルアカウントでログイン</h4>
                        </a>

                        <div class="social_btn_rap">
                            <button class="btn btn-default btn-fb social-btn" onclick="myFacebookLogin();">facebookログイン</button>
                            <a href="<?php echo $web_sso_base_url;?>/web/auth/process?sso_agent=2&web_sso_client_id=<?php echo $web_sso_client_id;?>&web_sso_application_id=<?php echo $web_sso_application_id;?>&base_url=<?php echo $base_url;?>">
                                <button class="btn btn-default btn-tw social-btn">twitterログイン</button>
                            </a>
                        </div>
                        <!-- /.social_btn_rap -->
                    </div>
                    <!-- /.login_by_social -->
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 login_fields">
                    <div class="login_by_social">
                        <a href="#">
                            <h4>メールアドレスでログイン</h4>
                        </a>
                        <div class="social_btn_rap">
                            <form action="" class="form-horizontal" id="Login_By_Email">
                                <div class="form-group">
                                    <input type="email" required class="form-control" name="email" id="email" placeholder="メールアドレス">
                                </div>
                                <!-- /.input-group -->
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control"  placeholder="パスワード" required  pattern=".{6,}" title="6 characters minimum" />
                                    <!-- /.form-control -->
                                </div>
                                <!-- /.input-group -->
                                <h4 class="text-center" style="font-weight:normal;"><a href="<?php echo $web_sso_base_url . '/web/password/reset?application_id='. $web_sso_application_id .'&client_id='.$web_sso_client_id.'&base_url='.$base_url;?>"> パスワードを忘れた方はこちら</a></h4>
                                <p class="error_container"></p>
                                <p class="text-center"><input type="submit" class="btn btn-default btn-blue" value="ログイン"></p>
                            </form>
                            <!-- /.form-horizontal -->
                        </div>
                        <!-- /.social_btn_rap -->
                    </div>
                    <!-- /.login_by_social -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.modal_sec -->
        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>
<!-- /.signup -->
<div class="modal_shadow" id="less_campus_point">
    <div id="modal_wrap" class="modal_main">
        <div class="modal_header">  <span class="close_modal">  </span></div>
        <!-- /.modal_header -->
        <div class="modal_body">
            <div class="modal_row clearfix">
                <div class="thanks_rap">
                    <h3 id="less_rank"></h3>
                    <h3 id="less_point">ポイントが足りません</h3>

                    <div class="home_btn">
                        <button class="btn btn-default btn-ash close_modal modal_btn">OK</button>
                    </div>
                    <!-- /.home_btn -->

                </div>
                <!-- /.thanks_rap -->
            </div>
            <!-- /.modal_sec -->


        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>

<!-- /.modal_shadow
User Information show after login the user
-->

<div class="modal_shadow" id="user_info">
    <div id="modal_wrap" class="modal_main">
        <div class="modal_header"> <span class="close_modal"> </span>  </div>
        <!-- /.modal_header -->
        <div class="modal_body">
            <p class="text-center subtitle"> ユーザー情報 </p>
            <div class="avatar"><a href="#">
                    <?php if($user_info[0]->icon=='' || empty($user_info[0]->icon)): ?>
                    <?php echo $this->Html->image('user.png', array('class'=>'','alt'=>'User Picture')); ?>
                    <?php else: ?>
                    <img class="img-circle" src="<?php echo $user_info[0]->icon ?>" alt="User avatar">
                    <?php endif; ?>
                </a>
            </div>
            <p class="username text-center">  <?php echo $user_info[0]->nickname; ?> <small>さん</small>  </p>
            <?php if($groupName == ''): ?>
            <p class="text-center group_exists"><a href="#" class="btn btn-default btn-blue btn-bordered">団体登録</a> </p>
            <?php endif ?>
            <div class="pointarea">
                <div class="groupname">
                    <?= $this->Html->image('emblem.png'); ?>
                    <span> <?= $groupName; ?></span>
                    <?php echo $this->Html->image("student_edit_btn.png", [
                        'url' => ['controller' => 'Home', 'action' => 'edit', $user_info[0]->id]
                    ]);
                    ?>
                </div>
                <!-- /.groupname -->
                <div class="point">
                    <p> <span> 所持ポイント </span> <span class="text-right"> <?= $campuse_point; ?> <small> pt </small></span></p>
                </div>
                <!-- /.point -->
            </div>
            <!-- /.pointarea -->
            <div class="button-area">
                <p class="text-center"><a href="<?php echo $web_sso_base_url . '/web/user/logout'; ?>" class="btn btn-default btn-blue"> ログアウト </a></p>
            </div>
            <!-- /.button-area -->
        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>

<div class="modal_shadow" id="invite_friend">
    <div id="modal_wraper" class="modal_main">
        <div class="modal_header">  <span class="close_modal">  </span></div>
        <div class="modal_body new_md">

        <div class="head_title">
            <h2>友人招待</h2>
            <p>友人を招待しましょう！</p>
        </div>
        <div class="input_from">
          <input type="text" name="" value="" placeholder="メールアドレスを入力">
          <div class="add_btn">
            <p>入力欄を追加</p>
            <a href="#">+</a>
          </div>
            <div class="clear_both">

            </div>
          <div class="checkbox_area">
            <input class="select_opt" type="checkbox" name="" value="" checked>
            <p class="ck_caption">上記の友人を自分の団体に追加</p>
          </div>
              <div class="submit_btn_sec">
                  <a href="#" class="submit_btn">送信</a>
              </div>
        </div>


        </div>
    </div>
</div>

<!-- /.modal_shadow
User Information show after login the user
-->
<div class="modal_shadow" id="group_notification">
    <div class="modal_main">
        <div class="modal_header"> <span class="close_modal">  </span></div>
        <!-- /.modal_header -->
        <div class="modal_body">
            <div id="preloader">
                <div id="status">&nbsp;</div>
            </div>
            <div class="beforeRegister">
                <h3 class="text-center color-blue title"> 団体登録 </h3>
                <p class="text-center subtitle"> 団体を登録しましょう！ </p>

                <p class="form_title"> ※ 団体名を選択または入力してください  </p>
                <div class="form_area row">
                    <form class="form-horizontal" id="studentGroupRegisterForm">
                        <?php if(count($allStudentGroupList) > 0 ): ?>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">団体名</label>
                            <div class="col-sm-10">
                                <select name="choice"  data-live-search="true" class="form-control studentGroupList">
                                    <option value="0" data-tokens="選択">選択</option>
                                    <?php foreach($allStudentGroupList as $group ): ?>
                                    <option value="<?php echo $group->id ?>" data-tokens="<?php echo $group->name ?>"><?php echo $group->name; ?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <!-- /# -->
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label org-label">団体名新規作成</label>
                            <div class="col-sm-10">
                                <input type="text" name="organization" class="form-control" placeholder="団体名を新規作成">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10 show_recent_group_name">

                            </div>
                            <div class="col-sm-offset-2 col-sm-10 text-center">
                                <button type="submit" class="btn btn-default login_byemail_btn"> 確認 </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.form_area -->
            </div>
            <!-- /.beforeRegister -->
            <div class="afterRegister">
                <p class="text-center"> 登録ありがとうございました! </p>
            </div>
            <!-- /.afterRegister -->
        </div>
        <!-- /.modal_body -->
    </div>
    <!-- /.modal_main -->
</div>
<!-- /.modal_shadow -->
<div class="modal"></div>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<?= $this->Html->script('owl_carousel/owl.carousel.min.js'); ?>
<?= $this->Html->script('bootstrap/bootstrap.min.js'); ?>
<?= $this->Html->script('bootstrap-select.min.js'); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<?= $this->Html->script('circlepass.js'); ?>
<?= $this->fetch('script') ?>
<script>


    window.fbAsyncInit = function () {
        FB.init({
            appId: "<?php echo $facebook_app_id;?>",
            xfbml: true,
            version: "<?php echo $facebook_app_version;?>"
        });
    };
    function myFacebookLogin() {
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                request_single_sign_on(response.authResponse.accessToken);
            } else {
                FB.login(function (response) {
                    if (response.authResponse) {
                        request_single_sign_on(response.authResponse.accessToken);
                    } else {
                        console.log('User cancelled login or did not fully authorize.');
                    }
                }, {scope: ''});
            }
        });
    }
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));



    function write_cookie(obj){
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + 1000*3600*24;
        now.setTime(expireTime);
        var cookie_domain = "<?php echo $cookie_domain;?>";
        document.cookie = "<?php echo $cookie_sc_oauth_access_token; ?>=" + obj.data.access_token + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        document.cookie = "sc_oauth_refresh_token=" + obj.data.refresh_token + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        document.cookie = "sc_user_id=" + obj.data.user_data.user_id + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        document.cookie = "<?php echo $cookie_sc_app_id ?>=" + obj.data.user_data.app_id + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        document.cookie = "web_sso_application_id=" + obj.data.web_sso_application_id + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        document.cookie = "web_sso_post_redirect=" + obj.data.web_sso_post_redirect + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;

        if (obj.data.user_data.facebook_id != null) {
            document.cookie = "sc_facebook_id=" + obj.data.user_data.facebook_id + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
            document.cookie = "sc_fb_token=" + obj.data.user_data.sc_fb_token + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
            document.cookie = "base_url=" + obj.data.user_data.base_url + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        }else if (obj.data.user_data.twitter_id != null) {
            document.cookie = "sc_twitter_id=" + obj.data.user_data.twitter_id + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        }else{
            document.cookie = "base_url=" + obj.data.user_data.base_url + "; expires="+now.toGMTString()+"; path=/; domain="+cookie_domain;
        }

        /*
         Check if the user already register in student group
         */
        $.ajax({
            url: url+'/checkUserExistsInGroup',
            type: 'post',
            data:{'user_id':obj.data.user_data.user_id},
            dataType: 'json',
            async:false,
//            contentType: "application/json; charset=utf-8",
//            cache: false,
            success: function (data) {
                console.log(data);
                if(data.success == false) {
                    document.cookie = "show_student_group_popup=1; expires=" + now.toGMTString() + "; path=/; domain=" + cookie_domain;


                }else {
                    document.cookie = "show_student_group_popup=2; expires=" + now.toGMTString() + "; path=/; domain=" + cookie_domain;
                }

            },
            error: function (request, status, error) {
                // This callback function will trigger on unsuccessful action
                alert("An error occurred-1: " + status + "nError: " + error + 'Response: '+request.responseText);
                console.log(status);
                console.log(request);

            }
        });

        window.location = obj.data.redirect_uri;
    }


    function request_single_sign_on(access_token) {
        $.ajax({
            type: 'POST',
            url: "<?php echo $web_sso_base_url;?>/web/auth/process",
            crossDomain: true,
            data: {
                sso_agent: 1,
                token: access_token,
                base_url: "<?php echo Router::fullbaseUrl(); ?>",
                web_sso_client_id: "<?php echo $web_sso_client_id;?>",
                web_sso_application_id: "<?php echo $web_sso_application_id;?>"
            },
            beforeSend: function () {
                $(".modal").show();
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                console.log(status);
                console.log(error);
            },
            complete: function () {
                $(".modal").hide();
            },
            success: function (data, textStatus, request) {
                var obj = JSON.parse(data);
                if (obj.success == true) {
                    write_cookie(obj);
                } else {
                    window.location.reload(true);
                }
            }
        });
    }


    $("#Login_By_Email").on('submit',function(){

        $.ajax({
            type: 'POST',
            url: "<?php echo $web_sso_base_url;?>/web/auth/process",
            crossDomain: true,
            data: {
                sso_agent: 3,
                email: $('#email').val(),
                password: $('#password').val(),
                base_url: "<?php echo Router::fullbaseUrl(); ?>",
                web_sso_client_id: "<?php echo $web_sso_client_id;?>",
                web_sso_application_id: "<?php echo $web_sso_application_id;?>"
            },
            beforeSend: function () {
                $(".modal").show();
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                console.log(status);
                console.log(error);
            },
            complete: function () {
                $(".modal").hide();
            },
            success: function (data, textStatus, request) {
                console.log(data);
                var obj = JSON.parse(data);
                if (obj.success == true) {
                    write_cookie(obj);
                } else {
                    $('.error_container').addClass('bg-danger text-danger').show();
                    $('.error_container').html(obj.data);
                    //window.location.reload(true);
                }
            }
        });

        return false;

    })

    //Check the student_group registration cookie set or not
    //If not registard then show the popup

    jQuery(document).ready(function(){


        setTimeout(function(){
            var cookie_domain = "<?php echo $cookie_domain;?>";
            var now = new Date();
            var time = now.getTime();
            var expire = time - 3600;
            now.setTime(expire);
            var value = $.cookie("show_student_group_popup");
            if(value==1) {
                $('#group_notification').oc_modal_show();

                document.cookie = 'show_student_group_popup=; Path=/; Expires='+now.toGMTString()+';domain='+cookie_domain;
            }
        },3000)

    $(".studentGroupList").selectpicker();

    })

    $(window).on('load', function() {
        //When first time visit the page open the review modal
        // Set cookie for one week
        // After one week again open the review modal
        var cookie_domain = "<?php echo $cookie_domain;?>";
        var now = new Date();
        var time = now.getTime();
        var expire = time + 7*24*60*60*1000;
        now.setTime(expire);
//        console.log(now.toGMTString());
        setTimeout(function(){
            var revieValue = $.cookie("review_modal");
            if(revieValue!=1){
                $("#review_modal").oc_modal_show();
                document.cookie = 'review_modal=1; Path=/; Expires='+now.toGMTString()+';domain='+cookie_domain;
            }
        },3000)
    });
</script>
</body>
</html>
