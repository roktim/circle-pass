<?php
/**
 * Created by Utpal.
 * User: Circle Pass
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
class SupportersTable extends Table
{

    public $alias = "Supporters";

    public function initialize(array $config)
    {
        $this->table('supporters');
    }
    /*
     * @param supporter_id
     * @author Utpal Biswas
     * */
	public function get_supporter_img($supporter_id){
		$query = $this->find()->select(['image'])
			->where(['id' => $supporter_id]);
        return  $query->first();
	}
    /*
     * @param supporter_id
     * @author Utpal Biswas
     * */
    public function get_supporter_name($supporter_id){
        $query = $this->find()->select(['name'])
            ->where(['id' => $supporter_id]);
        return  $query->first();
    }
}