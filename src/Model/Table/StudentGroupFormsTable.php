<?php
/**
 * Created by Utpal.
 * User: Circle Pass
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

class StudentGroupFormsTable extends Table
{

    public function initialize(array $config)
    {
        $this->table('student_group_forms');
        $this->__connection = ConnectionManager::get('default');
    }
    /*
     * @param supporter_id
     * @author Utpal Biswas
     * */
     public function form_names(){
         return $this->__connection->execute('SELECT * from student_group_forms ORDER BY id ASC')->fetchAll('assoc');
    }

}