<?php
/**
 * Created by Utpal.
 * User: Circle Pass
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
class CirclePassContentsTable extends Table
{

    public $alias = "CirclePassContents";

    public function initialize(array $config)
    {
        $this->table('circle_pass_contents');
    }
/*
 * @param $id
 * @created_by Utpal
 * get content details by supporter_id
 * */
    public function get_circle_pass_content_detail($id){

	   $query = $this->find()
		    ->where(['CirclePassContents.id' => $id ]);
	    return  $query->first();
    }
    /*
     *@Desc Get releated content
     * */
	public function get_related_content($id){
		$content = $this->get_circle_pass_content_detail($id);
		if(!empty($content->tag)){
			$tag = $content->tag;
			$query = $this->find()
				->where(['CirclePassContents.tag LIKE' => "%{$tag}%"])
        ->where(['CirclePassContents.disable =' => false])
				->where(['CirclePassContents.id <>' => $id])
				->all();
			return $query;
		} else {
			$query = $this->find()
        ->where(['CirclePassContents.disable' => false])
				->where(['CirclePassContents.id <>' => $id])
				->all();
			return $query;
		}

	}
	/*
	 * get popular content according to rank
	 *
	 * */
	public function get_popular_contents_by_rank(){
		$query = $this->find()
			->order(['rank' => 'DESC'])
			->limit(3);

		return $query->toArray();
	}
}
