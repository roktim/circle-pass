<?php
/**
 * Created by Utpal.
 * User: Circle Pass
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

class StudentGroupTypesTable extends Table
{

    public function initialize(array $config)
    {
        $this->table('student_group_types');
        $this->__connection = ConnectionManager::get('default');
    }
    /*
     * @param supporter_id
     * @author Utpal Biswas
     * */

     public function type_names(){
         return $this->__connection->execute('SELECT * from student_group_types ORDER BY id ASC')->fetchAll('assoc');
    }
}