<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class StudentGroupCategoryIdsTable extends Table
{
    public $alias = " StudentGroupCategoryIds";
    public function initialize(array $config)
    {
        $this->table('student_group_category_ids');
    }

  

}
