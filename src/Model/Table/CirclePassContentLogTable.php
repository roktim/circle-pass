<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class CirclePassContentLogTable extends Table
{
    public $alias = "CirclePassContentLog";

    public function initialize(array $config)
    {
        $this->table('circle_pass_content_logs');
    }


    public function get_student_groups_by_log_ids( $id ){

        $result = $this->find('all')
                ->hydrate(false)
                ->join([
                           'table' => 'student_groups',
                           'alias' => 'student_group',
                           'type' => 'RIGHT',
                           'conditions' => 'student_group.id = CirclePassContentLog.student_group_id',
                       ])
                ->where(['CirclePassContentLog.circle_pass_content_id'=>$id])
                ->select(['student_group.name','student_group.description','student_group.image','CirclePassContentLog.student_group_id','CirclePassContentLog.circle_pass_content_id'])
                ->toArray();

        if($result)
            return $result;
        return NULL;

    }
    /*
     *@param $id
     * @author Utpal Biswas
     * save log in the circle entry page
     * */
    public function save_log($content_id, $student_group_id){
        // Get the current time.
        $now = Time::now();

        $query = $this->query();
        $result = $query->insert(['circle_pass_content_id','student_group_id','confirmed','created','updated','disable'])
            ->values([
                'circle_pass_content_id' => $content_id,
                'student_group_id' => $student_group_id,
                'confirmed' => false,
                'created' => $now,
                'updated' => $now,
                'disable' => false,

            ])
            ->execute();
        if($result)
        return $result;

    }
}