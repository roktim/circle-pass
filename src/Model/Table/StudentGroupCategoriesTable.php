<?php
/**
 * Created by Utpal.
 * User: Circle Pass
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

class StudentGroupCategoriesTable extends Table
{
    public $alias = " StudentGroupCategories";
    public function initialize(array $config)
    {
        $this->table('student_group_categories');
        $this->__connection = ConnectionManager::get('default');
    }
    /*
     * @param supporter_id
     * @author Utpal Biswas
     * */
    public function student_group_categories_name(){
         return $this->__connection->execute('SELECT * from student_group_categories ORDER BY id ASC')->fetchAll('assoc');
    }

    public function student_group_categories_name_by_user($id){
         return $this->__connection->execute('SELECT student_group_category_id from student_group_category_ids where student_group_id= :id Order By id ASC', ['id' => $id])->fetchAll('assoc');
    }
    public function category_name_category_id($id){
        return $this->__connection->execute('SELECT name from student_group_categories where id= :id ', ['id' => $id])->fetchAll('assoc');
    }
}