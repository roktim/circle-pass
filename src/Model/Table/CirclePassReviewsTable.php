<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class CirclePassReviewsTable extends Table
{
    public $alias = "CirclePassReviews";

    public function initialize(array $config)
    {
        $this->table('circle_pass_reviews');
    }

    /*
     * Get all reviews with associated table fields
     * JOIN->student_group
     * INNER JOIN->University
     */
    public function get_all_reviews_by_id( $id ){

        $query = $this->find()
            ->hydrate(false)
            ->join([
                'student_group'=>[
                    'table' => 'student_groups',
                    'type'=>'LEFT',
                    'conditions' => 'student_group.id = CirclePassReviews.student_group_id'
                ],
                'univ'=>[
                    'table'=>'Univs',
                    'type'=>'LEFT',
                    'conditions'=>'student_group.univ_id=univ.id'
                ]
            ])
            ->select([
                        'CirclePassReviews.student_group_id','CirclePassReviews.title','CirclePassReviews.description',
                        'student_group.name','student_group.image','student_group.univ_id',
                        'univ.name'
                    ])
            ->where(['CirclePassReviews.id'=>$id])
             ->toArray();

        return $query;
    }

    /*
     * Get Only Student Group Ids
     */
    public function get_stuent_group_ids(){

        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'student_group_id'])->hydrate(false)->toArray();

        return array_filter($query);
    }
    /*
     * @Description content details page
     * @param $limit
     * */
    public function get_circle_pass_student_groups($limit){
	    $student_group_ids = $this->get_stuent_group_ids();
	    $id = array_rand($student_group_ids);
	    $query = $this->find('all')
		    ->hydrate(false)
		    ->join(['student_group'=>[
				           'table' => 'student_groups',
				           'type'=>'LEFT',
				           'conditions' => 'student_group.id = CirclePassReviews.student_group_id']
		           ])
		    ->select([ 'CirclePassReviews.student_group_id','CirclePassReviews.title','CirclePassReviews.description','student_group.name','student_group.image'])
		  // ->where(['CirclePassReviews.id'=>$id])
		    //->order('rand()')
		    ->limit($limit)
		    ->toArray();
	    return $query;
    }
}