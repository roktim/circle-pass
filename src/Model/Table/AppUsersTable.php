<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class AppUserTable extends Table
{
    public $alias = "AppUsers";

    public function initialize(array $config)
    {
        $this->table('app_users');
    }

    //Get app profile id using user id
    public function get_profile_id($id){
        $query = $this->find()
            ->where(['app_users.id' => $id ]);
        return  $query->first();
    }

    /*
     * Get App User by ID
     * Author: Sahid
     * Author website: sahid.me
     */
    public function get_appUser_by_ID( $user_id ){
        $query = $this->find()
            ->where(['app_user.id'=> $user_id])
            ->limit(1)
            ->toArray();
        return $query;
    }

}