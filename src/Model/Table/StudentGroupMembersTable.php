<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 8/4/16
 * Time: 12:42 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

class StudentGroupMembersTable extends Table
{
    public $alias = "StudentGroupMembers";

    public function initialize(array $config)
    {
        $this->table('student_group_members');
        $this->__connection = ConnectionManager::get('default');
    }

    public function save_member($user_id, $student_group_id)
    {
      $now = Time::now();

      $query = $this->query();
      $result = $query->insert(['user_id','student_group_id','created','updated'])
          ->values([
              'user_id' => $user_id,
              'student_group_id' => $student_group_id,
              'created' => $now,
              'updated' => $now
          ])
          ->execute();
      if($result)
      return $result;
    }


        /*
     * Get Student Edit All Info By ID
     * @param id integer id
     */
    public function student_group_edit($id)
    {

        return $this->__connection->execute('SELECT stg.image AS image,stg.description AS description,stg.member_number AS member_number,stg.location AS location,stgf.name As form_name,stgf.id As form_id,stgt.name As type_name,stgt.id As type_id,stgc.name AS category_name,stgc.id AS category_id,stg.phone_number As phone_number,stg.email AS email,stg.facebook_url AS facebook_url,stg.twitter_url AS twitter_url,stg.line_name AS line_name,stg.official_url AS official_url FROM student_groups stg LEFT JOIN student_group_members stgm ON stg.id=stgm.student_group_id LEFT JOIN app_users appu ON stgm.user_id = appu.id LEFT JOIN student_group_types stgt ON stg.student_group_type_id=stgt.id LEFT JOIN student_group_forms stgf ON stg.student_group_form_id=stgf.id LEFT JOIN student_group_category_ids stgci ON stg.id=stgci.student_group_id LEFT JOIN student_group_categories stgc ON stgc.id=stgci.student_group_category_id where stgm.user_id= :id', ['id' => $id])->fetch('assoc');

    }


  public function student_group_members($id)
  {

        return $this->__connection->execute('SELECT position,image from student_group_members  where user_id= :id', ['id' => $id])->fetchAll('assoc');
  }

  public function student_id($id)
  {

        return $this->__connection->execute('SELECT stgm.student_group_id AS id, stg.name AS name from student_groups AS stg LEFT JOIN student_group_members AS stgm ON stg.id=stgm.student_group_id where stgm.user_id= :id', ['id' => $id])->fetch('assoc');
  }


}
