<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class AppProfileTable extends Table
{
    public $alias = "AppProfiles";

    public function initialize(array $config)
    {
        $this->table('app_profiles');
    }

    /*
     * Get App User by ID
     * Author: Sahid
     * Author website: sahid.me
     */
    public function get_appProfile_by_ID( $user_id ){
        $query = $this->find()
            ->where(['app_profiles.app_user_id'=> $user_id])
            ->limit(1)
            ->toArray();
        return $query;
    }

}