<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/10/16
 * Time: 4:01 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class StudentGroupsTable extends Table
{
    public $alias = "StudentGroups";
    public function initialize(array $config)
    {
         $this->hasMany('CirclePassContentLog');
    }
    /*
     * Select Student Groups by ID
     * LEFT JOIN->Univ
     */
    public function get_group_by_circle_pass( $id ){
        $query = $this->find()
            ->hydrate(false)
            ->join([
                'table' => 'Univs',
                'alias' => 'univ',
                'type' => 'LEFT',
                'conditions' => 'univ.student_group_id = StudentGroups.univ_id',
            ])
            ->where(['StudentGroups.id'=>$id])
            ->select(['univ.name','StudentGroups.name'])
            ->toArray();

        return $query;
    }
/*
 * Get campus point and rate from student group according to user id
 * @param user id
 * @Author <Utpal Biswas>
 * */
    public function get_campus_point( $id ){
        $query = $this->find()
            ->hydrate(false)
            ->join([
                'c' => [
                    'table' => 'student_group_members',
                    'type' => 'INNER',
                    'conditions' => [
                                    'c.student_group_id = StudentGroups.id',
                                    'c.user_id'=>$id
                                    ],
                ]
            ])
            ->select(['StudentGroups.campus_point','StudentGroups.rate','c.permission'])
            ->toArray();

        return $query;
    }

    /*
     * Student user information
     * Author: sahid
     * Join student_member, student_groups, app_users
     */
    public function user_info( $user_id ){

        $query = $this->find()
            ->hydrate(false)
            ->join([
                'c' => [
                    'table' => 'student_group_members',
                    'type' => 'RIGHT',
                    'conditions' => [
                        'c.user_id = '.$user_id,
                        'c.student_group_id = StudentGroups.id'
                    ],
                    'limit'=>1
                ],
                'd'=>[
                    'table'=>'app_users',
                    'type'=>'INNER',
                    'conditions'=>[
                        'd.id'=>$user_id
                    ]
                ],
            ])
            ->select(['d.nickname','StudentGroups.campus_point','StudentGroups.name'])
            ->toArray();

        return $query;
    }


    public function save_group($groupName)
    {
      $now = Time::now();

      $query = $this->query();
      $result = $query->insert(['name','created','updated'])
          ->values([
              'name' => $groupName,
              'created' => $now,
              'updated' => $now
          ])
          ->execute();

      $query = $this->find('all', [
        'order' => ['StudentGroups.id' => 'DESC']
      ]);
      $row = $query->first();

      if($result)
      return $row;
    }
    /*
     * Get student group details
     * @Author <Utpal Biswas>
     * Join Student group and Univ
     * */
    public function profile_details($id){
        $query = $this->find()
            ->hydrate(false)
            ->join([
                       'table' => 'Univs',
                       'alias' => 'univ',
                       'type' => 'LEFT',
                       'conditions' => 'univ.id = StudentGroups.univ_id',
                   ])
            ->where(['StudentGroups.id'=>$id])
            ->select(['univ.name', 'StudentGroups.name', 'StudentGroups.image', 'StudentGroups.rate', 'StudentGroups.description', 'StudentGroups.official_url', 'StudentGroups.facebook_url', 'StudentGroups.twitter_url'])
            ->toArray();
        return $query;
    }
  

}
