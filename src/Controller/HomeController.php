<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/11/16
 * Time: 1:59 PM
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');/*This line loads the required RequestHandler Component*/
        $this->loadComponent('Paginator');
    }


    /*
     * Circle pass index
     * Face data as direct and with ajax request
     * Author: sahidhossen
     * Author URl: sahid.me
     */
    public function index()
    {
        $this->loadModel('Advertises');
        $this->loadModel('StudentGroups');
        $this->loadModel('CirclePassReviews');
        $this->loadModel('CirclePassContents');
        $this->loadModel('Supporters');
        $this->loadComponent('Utility');
        $advertises = $this->Advertises->find('all', array('conditions' => array('not' => array('advertises.advertise_type_id' => null))))->toArray();

        $student_group_ids = $this->CirclePassReviews->get_stuent_group_ids();
        $current_id = array_rand($student_group_ids);
        $reviews = $this->CirclePassReviews->get_all_reviews_by_id($current_id);





        $this->paginate = [
            'limit' => 6,
            'order' => [
                'CirclePassContents.id' => 'desc'
            ],
            'join' => array(
                'supporter' => [
                    'table' => 'supporters',
                    'type' => 'LEFT',
                    'conditions' => 'supporter.id = CirclePassContents.supporter_id'
                ]
            ),
            'conditions' => 'CirclePassContents.disable = false',
            'fields' => array('supporter.id', 'supporter.name', 'supporter.image', 'CirclePassContents.id', 'CirclePassContents.title', 'CirclePassContents.sub_title', 'CirclePassContents.rank', 'CirclePassContents.description', 'CirclePassContents.image', 'CirclePassContents.tag')

        ];

        //Ajax Request
        if ($this->request->is('ajax')) {
            $main_cat = $this->request->data('main_cat');
            $diff_cat = $this->request->data('diff_cat');

            if ($main_cat == 0 && $diff_cat == 0) {
                $condition = 'CirclePassContents.disable = false';
            }
            if ($main_cat && $diff_cat) {
                $condition = ['OR' => array(
                    array('CirclePassContents.categories LIKE' => "%" . $main_cat . "%"),
                    array('CirclePassContents.categories LIKE' => "%" . $diff_cat . "%")
                ), 'CirclePassContents.disable = false'];
            }

            if ($diff_cat && $main_cat == '0')
                $condition = ['CirclePassContents.categories LIKE' => "%" . $diff_cat . "%", 'CirclePassContents.disable = false'];

            if ($main_cat && $diff_cat == '0')
                $condition = ['CirclePassContents.categories LIKE' => "%" . $main_cat . "%", 'CirclePassContents.disable = false'];

            $this->paginate = [
                'limit' => 6,
                'conditions' => $condition,
                'order' => [
                    'CirclePassContents.id' => 'desc'
                ],

                'join' => array(
                    'supporter' => [
                        'table' => 'supporters',
                        'type' => 'LEFT',
                        'conditions' => 'supporter.id = CirclePassContents.supporter_id'
                    ]
                ),
                'fields' => array('supporter.id', 'supporter.name', 'supporter.image', 'CirclePassContents.id', 'CirclePassContents.title', 'CirclePassContents.sub_title', 'CirclePassContents.rank', 'CirclePassContents.description', 'CirclePassContents.image', 'CirclePassContents.tag')

            ];

            $this->viewBuilder()->layout('ajax_student_group');
        }
        $contents = $this->paginate('CirclePassContents');
        $this->set("web_img_base_url", Configure::read('Circle.web_img_base_url'));
        $this->set('title', "Circlepass｜Scサークル会員のための特典プラットフォーム");
        $this->set(compact('advertises', 'contents', 'reviews', 'order'));
    }


    /*
     * Details page view content
     */
    public function details($id)
    {
        $this->loadModel('CirclePassContents');
        $this->loadModel('Supporters');
        $this->loadModel('CirclePassReviews');
        $this->loadModel('StudentGroups');
        $this->loadModel('CirclePassContentLog');
        $content_title = "";
        $reviews = $this->CirclePassContentLog->get_student_groups_by_log_ids($id);
        $details = $this->CirclePassContents->get_circle_pass_content_detail($id);
        if (!empty($details->wide_image)) {
            $top_images[] = $details->wide_image;
        } else {
            $top_images[] = $details->image;
        }
        if (!empty($details->top_image_2)) {
            $top_images[] = $details->top_image_2;
        }
        if (!empty($details->top_image_2)) {
            $top_images[] = $details->top_image_3;
        }
        if (!empty($details->supporter_id)) {
            $supporter = $this->Supporters->get_supporter_img($details->supporter_id);
        }
        //Get user id from cookie after login
        $user_id = "";
        $campus_point = "";
        $student_group_rate = "";
        if (isset($_COOKIE['sc_user_id'])) {
            $user_id = $_COOKIE['sc_user_id'];
        }
        if (!empty($user_id)) {
            $result = $this->StudentGroups->get_campus_point($user_id);

            $campus_point = isset($result['0']['campus_point']) ? $result['0']['campus_point'] : 0;
            $student_group_rate = $result['0']['rate'];
            $permission = $result['0']['c']['permission'];

        }
        $related_content = $this->CirclePassContents->get_related_content($id);

        if (count($related_content) < 1 || $related_content == false) {
            $related_content = $this->CirclePassContents->get_popular_contents_by_rank();
        }
        if (!empty($details->title)) $content_title = $details->title;

        //force permission
        if(Configure::read('Circle.no_permission')) $permission = 1;

        $this->set('title', "Circle Pass | " . $content_title . "");
        $this->set(compact('details', 'related_content', 'supporter', 'reviews', 'user_id', 'campus_point', 'student_group_rate', 'top_images','permission'));
        $this->set("web_img_base_url", Configure::read('Circle.web_img_base_url'));
    }


    /*
     * Ajax call for the review section
     * Randomly select the review IDs
     */
    public function reviews()
    {
        $this->autoRender = false;

        $this->loadModel('StudentGroups');
        $this->loadModel('CirclePassReviews');
        $this->loadComponent('Utility');
        $student_group_ids = $this->CirclePassReviews->get_stuent_group_ids();

        $current_id = array_rand($student_group_ids);

        $current_std_info = $this->CirclePassReviews->get_all_reviews_by_id($current_id);
        $base_url = Configure::read('Circle.IMAGE_BASE_URL');

        if (!empty($current_std_info[0]['student_group']['image'])) {
            $current_std_info[0]['student_group']['image_url'] = $base_url . $current_std_info[0]['student_group']['image'];
            $current_std_info[0]['student_group']['image_alt'] = $current_std_info[0]['student_group']['name'];
            $fle_exists = $this->Utility->url_exists($current_std_info[0]['student_group']['image_url']);

            if (empty($fle_exists)) {
                $current_std_info[0]['student_group']['image_url'] = $base_url . 'img/no-image.png';
                $current_std_info[0]['student_group']['image_alt'] = 'No Image';
            }
        } else {
            $current_std_info[0]['student_group']['image_url'] = $base_url . 'img/no-image.png';
            $current_std_info[0]['student_group']['image_alt'] = 'No Image';
        }


        $data = array();

        if (count($current_std_info) > 0) {
            $data['error'] = false;
            $data['message'] = $current_std_info[0];
            $data['final_img'] = $current_std_info[0]['student_group']['image_url'];
           // print_r( $data['img']);

        } else {
            $data['error'] = true;
            $data['message'] = 'There is no data';
        }
        echo json_encode($data);

    }

    /*
     * Review list for mobile only
     */
    public function reviewlist()
    {
        $this->loadModel('StudentGroups');
        $this->loadModel('CirclePassReviews');
        $this->loadComponent('Utility');
        $allReviews = $this->CirclePassReviews->get_circle_pass_student_groups(100);
        $i = 0;
        $base_url = Configure::read('Circle.IMAGE_BASE_URL');
        foreach ($allReviews as $item) {
            if (!empty($item['student_group']['image'])) {
                $allReviews[$i]['student_group']['image_url'] = $base_url . $item['student_group']['image'];
                $allReviews[$i]['student_group']['image_alt'] = $item['student_group']['name'];
                $fle_exists = $this->Utility->url_exists($allReviews[$i]['student_group']['image_url']);
                if (empty($fle_exists)) {
                    $allReviews[$i]['student_group']['image_url'] = $base_url . 'img/default-thumbnail.jpg';
                    $allReviews[$i]['student_group']['image_alt'] = 'No Image';
                }
            } else {
                $allReviews[$i]['student_group']['image_url'] = $base_url . 'img/default-thumbnail.jpg';
                $allReviews[$i]['student_group']['image_alt'] = 'No Image';
            }
            $i++;
        }
        $allReviews = array_values(array_unique($allReviews));
        $this->set('title', "Circle Pass | Review");
        $this->set(compact('allReviews'));
    }

    /*
     * Student Group Datas
     */
    public function all_student_groups()
    {
        $this->autoRender = false;

        $this->loadModel('StudentGroups');
        $this->paginate = [
            'limit' => 25,
            'order' => [
                'Articles.title' => 'asc'
            ]
        ];
        $studentss = $this->paginate($this->StudentGroups);
    }

    /*
     *@param $id
     * @author Utpal Biswas
     * @description Entry circle circle pass content
     * */
    public function entryContent($id)
    {
        $this->loadModel('CirclePassContents');
        $this->loadModel('Supporters');
        $this->loadModel('CirclePassContentLog');
        $this->loadModel('StudentGroups');
        $this->loadModel('AppProfiles');

        $group_info = null;
        $details = $this->CirclePassContents->get_circle_pass_content_detail($id);
        if (!empty($details->wide_image)) {
            $top_images[] = $details->wide_image;
        } else {
            $top_images[] = $details->image;
        }
        if (!empty($details->top_image_2)) {
            $top_images[] = $details->top_image_2;
        }
        if (!empty($details->top_image_2)) {
            $top_images[] = $details->top_image_3;
        }
        if (isset($_COOKIE['sc_user_id'])) {

            $user_id = $_COOKIE['sc_user_id'];

            $current_user = $this->AppUsers->find()
                ->where(['AppUsers.id' => $user_id])
                ->select(['AppUsers.nickname'])
                ->toArray();

            $app_profile = $this->AppProfiles->find()
                ->where(['AppProfiles.app_user_id' => $user_id])
                ->select(['AppProfiles.email'])
                ->select(['AppProfiles.phone_number'])
                ->toArray();

            $group_info = $this->StudentGroups->find()
                ->join([
                    'c' => [
                        'table' => 'student_group_members',
                        'type' => 'INNER',
                        'conditions' => [
                            'c.user_id' => $user_id,
                            'StudentGroups.id = c.student_group_id'
                        ]
                    ]
                ])
                ->select(['StudentGroups.id', 'StudentGroups.name', 'StudentGroups.location', 'StudentGroups.email','StudentGroups.phone_number'])
                ->toArray();
        } else {
           $this->redirect($this->referer());
       }

        if ($this->request->is('ajax')) {
            $point = $this->request->data('point');
            $title = $this->request->data('title');

            $groupId = null;
            $groupName = '';
            $groupLocation = '';
            $groupEmail = '';
            if (count($group_info) > 0) {
                foreach ($group_info as $info) {
                    $groupId = $info->id;
                    $groupName = $info->name;
                    $groupLocation = $info->location;
                    $groupEmail = $info->email;
                }
                $this->CirclePassContentLog->save_log($id, $groupId);
            }

            /*Send mail*/
            $supportMail = Configure::read('SupportMail');
            $studentGroupDetailURL = Configure::read('Admin.group_detail');

            $now = date("Y-m-d h:i:s a", time());
            $body = "以下の内容で申し込みがありました。\n\nコンテンツ名：" . $title . "\n";
            $body .= "申請者：" . $current_user[0]['nickname'] . "\n";
            $body .= "団体名：" . $groupName . "\n";
            $body .= "活動場所：" . $groupLocation . "\n";
            $body .= "団体詳細：" . $studentGroupDetailURL . $groupId . "\n";
            $body .= "連絡先：" . $groupEmail . "\n\n";
            $body .= "申請日時：" . $now . "\n";
            $body .= "消費ポイント：" . $point . "\n";
            $email = new Email('default');

            $email->from(['support@oceanize.co.jp' => 'Circlepass'])
                ->to($supportMail)
                ->subject('Circle Passへの申し込みがありました')
                ->send($body);

            echo "Success";
            die();
        }
        $details = $this->CirclePassContents->get_circle_pass_content_detail($id);
        if (!empty($details->supporter_id)) {
            $supporter = $this->Supporters->get_supporter_img($details->supporter_id);
        }
        $user_id = "";
        $campus_point = "";
        if (isset($_COOKIE['sc_user_id'])) {
            $user_id = $_COOKIE['sc_user_id'];
        }
        if (!empty($user_id)) {
            $result = $this->StudentGroups->get_campus_point($user_id);
            $campus_point = $result['0']['campus_point'];
        }

        $this->set('title', "Circle Pass: Entry Page ");
        $this->set(compact('group_info','app_profile','details', 'related_content', 'supporter', 'campus_point', 'top_images'));
    }

   /*
   * @author Utpal Biswas
   * @description Student Group Profile page
   * */
    public function profile($id){

        $this->loadModel('StudentGroups');
        $this->loadModel('StudentGroupCategories');
        $this->loadModel('StudentGroupCategoryIds');
        $this->loadComponent('Utility');
        $details = $this->StudentGroups->profile_details($id);

        $base_url = Configure::read('Circle.web_img_base_url');
        if (!empty($details[0]['image'])) {
            $details[0]['image_url'] = $base_url . $details[0]['image'];
            $fle_exists = $this->Utility->url_exists($details[0]['image_url']);

            if (empty($fle_exists)) {
                $details[0]['image_url'] = $base_url . 'img/no-image.png';
            }
        } else {
            $details[0]['image_url'] = $base_url . 'img/no-image.png';
        }

        $cat_name = [];
        $categories = $this->StudentGroupCategoryIds->find()
            ->where(['StudentGroupCategoryIds.student_group_id' => $id])
            ->select(['StudentGroupCategoryIds.student_group_category_id'])
            ->toArray();

        if(count($categories) > 0){
            foreach ($categories as $ct){

                if(!empty($ct->student_group_category_id)){
                    $categories_name = $this->StudentGroupCategories->category_name_category_id($ct->student_group_category_id);
                    if(!empty($categories_name)){
                        array_push($cat_name, $categories_name[0]['name']);
                    }
                }
            }
        }

        $this->set('title', "Circle Pass | Student Group Profile");
        $this->set(compact('details', 'cat_name'));
    }

    /*
     * USER INFORMATION
     * author: Sahid
     */

    public function beforeFilter(Event $event)
    {

        $current_user = $group_info = array();

        $this->loadModel('StudentGroups');
        $this->loadModel('AppUsers');

        $allStudentGroupList = $this->StudentGroups->find('all');

        if (isset($_COOKIE['sc_user_id'])) {

            $user_id = $_COOKIE['sc_user_id'];

            $current_user = $this->AppUsers->find()
                ->where(['AppUsers.id' => $user_id])
                ->select(['AppUsers.nickname', 'AppUsers.icon','AppUsers.id'])
                ->toArray();

            $group_info = $this->StudentGroups->find()
                ->join([
                    'c' => [
                        'table' => 'student_group_members',
                        'type' => 'INNER',
                        'conditions' => [
                            'c.user_id' => $user_id,
                            'StudentGroups.id = c.student_group_id'
                        ]
                    ]
                ])
                ->select(['StudentGroups.id', 'StudentGroups.name', 'StudentGroups.campus_point', 'StudentGroups.rate'])
                ->toArray();

        }

        $groupId = null;
        $groupName = '';
        $campuse_point = 0;
        $rank = null;
        if (count($group_info) > 0) {
            foreach ($group_info as $info) {
                $groupId = $info->id;
                $groupName = $info->name;
                $campuse_point = $info->campus_point;
                $rank = $info->rate;
            }
        }

        $this->set('user_info', $current_user);
        $this->set('groupId', $groupId);
        $this->set('groupName', $groupName);
        $this->set('campuse_point', $campuse_point);
        $this->set('student_rank', $rank);
        $this->set('allStudentGroupList', $allStudentGroupList);

    }


    /*
     * Check if user exists
     */
    public function _userExistsInGroup($user_id)
    {
        $this->loadModel('StudentGroups');

        $group_info = $this->StudentGroups->find()
            ->join([
                'c' => [
                    'table' => 'student_group_members',
                    'type' => 'INNER',
                    'conditions' => [
                        'c.user_id' => $user_id,
                        'StudentGroups.id = c.student_group_id'
                    ]
                ]
            ])
            ->select(['StudentGroups.name', 'StudentGroups.campus_point'])
            ->toArray();

        return $group_info;
    }

    /*
     * Check if the user already members by student group
     * Author: Sahid Hossen
     * Author Link: sahid.me
     */
    public function checkUserExistsInGroup()
    {

        $this->autoRender = false;
        $user_id = $this->request->data('user_id');

        $group_info = $this->_userExistsInGroup($user_id);

        $result['data'] = count($group_info);

        if (count($group_info) > 0) {
            $result['success'] = true;
        } else {
            $result['success'] = false;
        }

        echo json_encode($result);
    }

    /*
     * Register student group members
     * @params : student_group_ids, app_userID, StudentGroupName
     */
    public function registerStudentGroup()
    {

        $this->loadModel('StudentGroups');
        $this->loadModel('StudentGroupMembers');
        $now = Time::now();
        $this->autoRender = false;

        $user_id = $_COOKIE['sc_user_id'];
        $GroupID = $this->request->data('GroupID');
        $groupName = $this->request->data('NewGroupName');

        $result = array();
        if (!empty($groupName) || $groupName != '') {

            //Create New Student Group
            $NewGroup = $this->StudentGroups->save_group($groupName);
            $GroupID = $NewGroup->id;

            //Create new student Member
            if ($d = $this->StudentGroupMembers->save_member($user_id, $GroupID)) {
                $result['success'] = true;
            } else {
                $result['success'] = false;
            }

        } else {

            //Member Registration
            if ($d = $this->StudentGroupMembers->save_member($user_id, $GroupID)) {
                $result['success'] = true;
            } else {
                $result['success'] = false;
            }
        }

        echo json_encode($result);
    }
  
    public function edit($id){
        if (isset($_COOKIE['sc_user_id'])) {
        try {
            $this->loadComponent('StudentGroupMembers');
            $this->loadComponent('StudentGroupCategories');
            $this->loadComponent('StudentGroupForms');
            $this->loadComponent('StudentGroupTypes');
            $id = $_COOKIE['sc_user_id'];
            $use_id = $this->StudentGroupMembers->student_id($id);
            $student_group_name = $use_id['name'];

            $student_edit = $this->StudentGroupMembers->student_edit_info($id);
            $student_gr_members = $this->StudentGroupMembers->student_group_members($id);
            $cat = $this->StudentGroupCategories->student_group_categories_name();
            $cat_by_user = $this->StudentGroupCategories->student_group_categories_name_by_user($use_id['id']);
            $forms= $this->StudentGroupForms->form_name();
            $types= $this->StudentGroupTypes->type_name();
        }   catch (Exception $e) {
            $this->Flash->set($e->getMessage());
        }
        $this->set('title', 'Circlepass｜Scサークル会員のための特典プラットフォーム');
        $this->set(compact('student_edit','cat','student_gr_members','forms','types','cat_by_user', 'student_group_name'));
        }else {
            $this->redirect($this->referer());
        }
    }

    public function profileUpdate(){

        $this->loadModel('StudentGroups');
        $this->loadModel('StudentGroupCategoryIds');
        $this->loadComponent('StudentGroupMembers');
        $id = $_COOKIE['sc_user_id'];
        $use_id = $this->StudentGroupMembers->student_id($id);

        if(!empty($this->request->data['photo'])){

        $newdata['photo']= $this->request->data['photo'];

        list($type, $newdata['photo']) = explode(';', $newdata['photo']);
        list(, $newdata['photo'])      = explode(',', $newdata['photo']);
        $newdata['photo'] = base64_decode($newdata['photo']);
        $newdata['img_name'] = $id.'.png';
        file_put_contents( WWW_ROOT . 'img/group_img/'.$newdata['img_name'], $newdata['photo']);
       

        

        $newdata['category_id'] = $this->request->data['category_id'];
        $newdata['description'] = $this->request->data['description'];
        $newdata['member_number'] = $this->request->data['member_number'];
        $newdata['location'] = $this->request->data['location'];
        $newdata['form_name'] = $this->request->data['form_name'];
        $newdata['type_name'] = $this->request->data['type_name'];
        $newdata['phone_number'] = $this->request->data['phone_number'];
        $newdata['facebook_url'] = $this->request->data['facebook_url'];
        $newdata['line_name'] = $this->request->data['line_name'];
        $newdata['email'] = $this->request->data['email'];
        $newdata['twitter_url'] = $this->request->data['twitter_url'];
        $newdata['official_url'] = $this->request->data['official_url'];
        $fields = [

            'image'        =>$newdata['img_name'],
            'description' => $newdata['description'],
            'member_number' => $newdata['member_number'],
            'location' => $newdata['location'],
            'student_group_form_id' => $newdata['form_name'],
            'student_group_type_id' => $newdata['type_name'],
            'phone_number' => $newdata['phone_number'],
            'facebook_url' => $newdata['facebook_url'],
            'line_name' => $newdata['line_name'],
            'email' => $newdata['email'],
            'twitter_url' => $newdata['twitter_url'],
            'official_url' => $newdata['official_url']
        ];

        }else{
        
        $newdata['category_id'] = $this->request->data['category_id'];
        $newdata['description'] = $this->request->data['description'];
        $newdata['member_number'] = $this->request->data['member_number'];
        $newdata['location'] = $this->request->data['location'];
        $newdata['form_name'] = $this->request->data['form_name'];
        $newdata['type_name'] = $this->request->data['type_name'];
        $newdata['phone_number'] = $this->request->data['phone_number'];
        $newdata['facebook_url'] = $this->request->data['facebook_url'];
        $newdata['line_name'] = $this->request->data['line_name'];
        $newdata['email'] = $this->request->data['email'];
        $newdata['twitter_url'] = $this->request->data['twitter_url'];
        $newdata['official_url'] = $this->request->data['official_url'];
        $fields = [
            'description' => $newdata['description'],
            'member_number' => $newdata['member_number'],
            'location' => $newdata['location'],
            'student_group_form_id' => $newdata['form_name'],
            'student_group_type_id' => $newdata['type_name'],
            'phone_number' => $newdata['phone_number'],
            'facebook_url' => $newdata['facebook_url'],
            'line_name' => $newdata['line_name'],
            'email' => $newdata['email'],
            'twitter_url' => $newdata['twitter_url'],
            'official_url' => $newdata['official_url']
        ];
    }

        $conditions = [
            'id' => $use_id['id']
        ];
        $this->StudentGroups->updateAll($fields,$conditions);


        $catData = [];
        $date=date("Y-m-d h:i:s");
        foreach ($newdata['category_id'] as $key => $value) {
         $catData [$key]['student_group_id'] = $use_id['id'];
         $catData [$key]['student_group_category_id'] = $value;
         $catData [$key]['created'] = $date;
        }
        $this->StudentGroupCategoryIds->deleteAll(array('student_group_id'=>$use_id['id']));
        $catData = $this->StudentGroupCategoryIds->newEntities($catData);
        $this->StudentGroupCategoryIds->saveMany($catData);

    }
}

