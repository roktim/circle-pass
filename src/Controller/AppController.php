<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie', ['domain' => Configure::read('Circle.cookie_domain')]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        $this->set("facebook_app_id", Configure::read('Circle.facebook_app_id'));
        $this->set("facebook_app_version", Configure::read('Circle.facebook_app_version'));
        $this->set("web_sso_base_url", Configure::read('Circle.web_sso_base_url'));
        $this->set("web_img_base_url", Configure::read('Circle.web_img_base_url'));
        $this->set("web_sso_client_id", Configure::read('Circle.web_sso_client_id'));
        $this->set("web_sso_application_id", Configure::read('Circle.web_sso_application_id'));
        $this->set("base_url", Configure::read('Circle.base_url'));
        $this->set("cookie_domain", Configure::read('Circle.cookie_domain'));
        $this->set("cookie_sc_app_id", Configure::read('Circle.COOKIE_NAME_SC_APP_ID'));
        $this->set("cookie_sc_oauth_access_token", Configure::read('Circle.COOKIE_NAME_SC_OAUTH_ACCESS_TOKEN'));
    }

   /* public function beforeFilter(Event $event){
        debug(Configure::read('Circle.facebook_app_id'));
    }*/
}
