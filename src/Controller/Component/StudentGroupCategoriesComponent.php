<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class StudentGroupCategoriesComponent extends Component
{
    public $controller = null;
    public $components = ['Cookie'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /*
     * Check URL exists
     * @author Tahsin Hasan <http://newdailyblog.blogspot.com>
     */
   

    public static function student_group_categories_name()
    {   

    try {
            $StudentGroupCategoriesTable = TableRegistry::get('StudentGroupCategories');
            $result = $StudentGroupCategoriesTable->student_group_categories_name();
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }


    public static function student_group_categories_name_by_user($id)
    {   

    try {
            $StudentGroupCategoriesTable = TableRegistry::get('StudentGroupCategories');
            $result = $StudentGroupCategoriesTable->student_group_categories_name_by_user($id);
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }

}