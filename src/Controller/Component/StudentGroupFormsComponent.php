<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class StudentGroupFormsComponent extends Component
{
    public $controller = null;
    public $components = ['Cookie'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /*
     * Check URL exists
     * @author Tahsin Hasan <http://newdailyblog.blogspot.com>
     */


    public static function form_name()
    {   

    try {   $id = $_COOKIE['sc_user_id'];
            if(empty($id)) throw new Exception(__('invalid_param'));
            $StudentGroupFormsTable = TableRegistry::get('StudentGroupForms');
            $result =$StudentGroupFormsTable->form_names();
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }
}