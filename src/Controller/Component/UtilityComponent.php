<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

class UtilityComponent extends Component
{
    public $controller = null;
    public $components = ['Cookie'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /*
     * Check URL exists
     * @author Tahsin Hasan <http://newdailyblog.blogspot.com>
     */
    public static function url_exists($url)
    {
        $file_headers = @get_headers($url);
        if(strpos($file_headers[0], '404') !== false){
            return false;
        } else if (strpos($file_headers[0], '302') !== false && strpos($file_headers[0], '404') !== false){
            return false;
        } else {
            return true;
        }
    }
}