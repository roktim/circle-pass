<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class StudentGroupMembersComponent extends Component
{
    public $controller = null;
    public $components = ['Cookie'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /*
     * Check URL exists
     * @author Tahsin Hasan <http://newdailyblog.blogspot.com>
     */
    public static function student_edit_info($id)
    {   

    try {
            $id = $_COOKIE['sc_user_id'];
            if(empty($id)) throw new Exception(__('invalid_param'));
            $StudentGroupTable = TableRegistry::get('StudentGroupMembers');
            $result = $StudentGroupTable->student_group_edit($id);
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }

    public static function student_group_members($id)
    {   

    try {
            $id = $_COOKIE['sc_user_id'];
            if(empty($id)) throw new Exception(__('invalid_param'));
            $StudentGroupMembersTable = TableRegistry::get('StudentGroupMembers');
            $result = $StudentGroupMembersTable->student_group_members($id);
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }


    public static function student_id($id)
    {   

    try {
            $id = $_COOKIE['sc_user_id'];
            if(empty($id)) throw new Exception(__('invalid_param'));
            $StudentGroupMembersTable = TableRegistry::get('StudentGroupMembers');
            $result = $StudentGroupMembersTable->student_id($id);
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }
}