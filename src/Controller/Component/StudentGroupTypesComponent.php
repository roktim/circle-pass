<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class StudentGroupTypesComponent extends Component
{
    public $controller = null;
    public $components = ['Cookie'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /*
     * Check URL exists
     * @author Tahsin Hasan <http://newdailyblog.blogspot.com>
     */


    public static function type_name()
    {   

    try {   $id = $_COOKIE['sc_user_id'];
            if(empty($id)) throw new Exception(__('invalid_param'));
            $StudentGroupTypesTable = TableRegistry::get('StudentGroupTypes');
            $result =$StudentGroupTypesTable->type_names();
        } catch (Exception $e) {
            $result = array("success" => false, "error" => $e->getMessage());
        }
        return $result;
    }
}