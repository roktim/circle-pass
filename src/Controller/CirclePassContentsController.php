<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 5/11/16
 * Time: 1:59 PM
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CirclePassContentController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');/*This line loads the required RequestHandler Component*/
        $this->loadComponent('Paginator');
    }

}